#!/usr/bin/env bash

ng build --prod=true \
         --output-path ../backend/usr/share/pidns/pidns-web/ \
         --base-href /ui/ --aot=true \
         --buildOptimizer=true --optimization=true --sourceMap=false
