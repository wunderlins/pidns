export const environment = {
  production: true,
  apiUrl: "/api/1.0/dnsmasq/",
  interfaces: {
    refresh_interval_ms: 0, // milli seconds
  }
};
