import { Component, OnInit } from '@angular/core';
import { DnsmasqService } from '../shared/dnsmasq.service';
import { CfgDirective } from '../shared/cfg-directive.type';
import { CfgGroup } from '../shared/cfg-group.type';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-dnsmasq',
  templateUrl: './dnsmasq.component.html',
  styleUrls: ['./dnsmasq.component.css']
})
export class DnsmasqComponent implements OnInit {
  cfgdirective: CfgDirective[] = [];
  config: CfgDirective[];
  cfgGroup: CfgGroup[];
  constructor(private dnsmasqService: DnsmasqService) { }

  ngOnInit(): void {
    /**
     * Load Directives and configs together, 
     * then start loading groups.
     * 
     * Loading group will trigger the display.
     */
    //console.log(this.cfgdirective)
    if (this.cfgdirective.length == 0) {
      console.log("Loading full CfgDirective")
      combineLatest(
          this.dnsmasqService.getConfig(), this.dnsmasqService.getDirectives(),
          (c, d) => ({c, d}))
          .subscribe(pair => {
            this.config = pair.c;
            this.cfgdirective = pair.d; 
            //console.log(pair.d)
            //console.log("loaded")

            this.dnsmasq_fetch_cfggroups()
          })
     } else {
      console.log("Loading simple CfgDirective")
      this.dnsmasq_fetch_config()
      this.dnsmasq_fetch_cfggroups()
     }
  }

  dnsmasq_fetch_cfgdirective() {
    this.dnsmasqService.getDirectives()
      .subscribe(o => this.cfgdirective = o);
  }

  dnsmasq_fetch_config() {
    this.dnsmasqService.getConfig()
      .subscribe(o => this.config = o);
  }

  dnsmasq_fetch_cfggroups() {
    this.dnsmasqService.getGroups()
      .subscribe(o => this.cfgGroup = o);
  }

}
