import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DnsmasqComponent } from './dnsmasq.component';

describe('DnsmasqComponent', () => {
  let component: DnsmasqComponent;
  let fixture: ComponentFixture<DnsmasqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DnsmasqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DnsmasqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
