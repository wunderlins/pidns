import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DnsmasqConfigComponent } from './dnsmasq-config.component';

describe('DnsmasqConfigComponent', () => {
  let component: DnsmasqConfigComponent;
  let fixture: ComponentFixture<DnsmasqConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DnsmasqConfigComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DnsmasqConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
