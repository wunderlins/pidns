import { Component, Input, OnInit } from '@angular/core';
import { InterfacesService } from '../shared/interfaces.service';
import { DnsmasqService } from '../shared/dnsmasq.service';
import { CfgDirective } from '../shared/cfg-directive.type';
import { CfgGroup } from '../shared/cfg-group.type';
import { Ip } from '../shared/ip.type';
import { Interface } from '../shared/interface.type';
import { combineLatest } from 'rxjs';

export interface Checkbox {
  name: String;
  label: String;
  checked: Boolean;
  ip: Ip[];
}

export interface Domain {
  name: String;
  network: String;
}

export interface Range {
  start: String;
  end: String;
  lease_time: String;
}

export interface DhcpOption {
  name: String;
  value: String;
}

@Component({
  selector: 'app-dnsmasq-config',
  templateUrl: './dnsmasq-config.component.html',
  styleUrls: ['./dnsmasq-config.component.css']
})
export class DnsmasqConfigComponent implements OnInit {

  cfgdirective: CfgDirective[] = [];
  config: CfgDirective[];
  cfgGroup: CfgGroup[];

  // internal model
  interface_dns: Checkbox[] = [];
  interface_dhcp: Checkbox[] = [];
  interfaces: Interface[] = [];
  dhcp_options: Map<string, DhcpOption>[] =  [];
  dhcp_ranges: Range[] = [];

  @Input() server: String[] = [];
  @Input() zones: Domain[] = [];

  constructor(private dnsmasqService: DnsmasqService,
              private interfacesService: InterfacesService) {
    this.dhcp_options["ntp-server"] = {name: null, value: null};
    this.dhcp_options["router"]     = {name: null, value: null};
  }

  ngOnInit(): void {
    this.load();
  }

  load() {
    /**
     * Load Directives and configs together, 
     * then start loading groups.
     * 
     * Loading group will trigger the display.
     */
    combineLatest(
      this.interfacesService.getInterfaces(),
        this.dnsmasqService.getConfig(), 
        this.dnsmasqService.getDirectives(),
        this.dnsmasqService.getGroups(),
        (i, c, d, g) => ({i, c, d, g}))
        .subscribe(pair => {
          this.cfgdirective = pair.d; 
          this.cfgGroup = pair.g;
          this.process_fetch(pair.i);
          this.process_config(pair.c);
      }
    )
  }

  process_fetch(o) {
    this.interfaces = o;
    //console.log(o)
    for (let i in this.interfaces) {
      // interfaces
      let iface_dhcp: Checkbox = {
        name: this.interfaces[i].iface,
        label:  this.interfaces[i].iface,
        checked: true,
        ip: this.interfaces[i].ip_addr4
      }
      let iface_dns: Checkbox = {
        name: this.interfaces[i].iface,
        label:  this.interfaces[i].iface,
        checked: false,
        ip: this.interfaces[i].ip_addr4
      }
      this.interface_dns.push(iface_dns)
      this.interface_dhcp.push(iface_dhcp)
    }
    //console.log(this.interface)
  }

  save(configForm) {
    console.log(configForm);
    return true;
  }

  reset() {
    this.dnsmasqService.getConfig()
      .subscribe(o => this.process_config(o));
  }

  delete_zone(event: Event, i) {
    this.zones.splice(i, 1);
  }

  delete_dhcp_range(event: Event, i) {
    this.dhcp_ranges.splice(i, 1);
  }

  updateInterfacesDns(event: Event) {
    return true;
  }

  updateInterfacesDhcp(event: Event) {
    return true;
  }

  updateAddresses(event: Event) {
    return true;
  }
  
  onServerValuechange(event: Event) {
    return true
  }

  dnsmasq_fetch_cfgdirective() {
    this.dnsmasqService.getDirectives()
      .subscribe(o => this.cfgdirective = o);
  }

  process_config(val) {
    this.config = val;

    // select interfaces
    let s: String[] = [];
    let d: Domain[] = [];
    let r: Range[] = [];
    let o: Map<string, DhcpOption>[] = [];

    // reset
    for (let iface in this.interface_dns)
      this.interface_dns[iface].checked = false
    for (let iface in this.interface_dhcp)
      this.interface_dhcp[iface].checked = true


    for (let c in this.config) {
      let cfg = this.config[c]

      if (cfg.key == "server") {
        s.push(cfg.value)
        //console.log(cfg.value)
      }

      if (cfg.key == "domain") {
        let parts = cfg.value.split(",")
        let net = parts[1].split("/")
        let dom: Domain = {
          name: parts[0],
          network: net[0] + "/" + net[1]
        }
        d.push(dom)
      }

      if (cfg.key == "interface") {
        for (let iface in this.interface_dns) {
          if (this.interface_dns[iface].name == cfg.value)
            this.interface_dns[iface].checked = true
        }
      }

      if (cfg.key == "no-dhcp-interface") {
        for (let iface in this.interface_dhcp) {
          if (this.interface_dhcp[iface].name == cfg.value)
            this.interface_dhcp[iface].checked = false
        }
      }

      // dhcp ranges
      if (cfg.key == "dhcp-range") {
        let parts: String[] = cfg.value.split(",");
        let l = null;
        if (parts.length > 2) {
          l = parts[2]
        }
        r.push({
          start: parts[0],
          end: parts[1],
          lease_time: l
        })
      }

      // dhcp options
      if (cfg.key == "dhcp-option") {
        let parts: String[] = cfg.value.split(",");
        let opt: string[] = parts[0].split(":");

        if (opt.length == 2 && ["router", "ntp-server"].indexOf(opt[1]) != -1) {
          o[opt[1].toString()] =  {
            name: opt[1],
            value: parts[1]
          };
        }
      }


    }
    //console.log(o);
    // make sure we have all keys defined
    let options = ["router", "ntp-server"]
    for (let i in options) {
      //console.log(o[options[i]])
      if (!o[options[i]]) {
        o[options[i]] = {name: null, value: null};
      }
    }
    this.dhcp_options = o;
    this.dhcp_ranges = r;
    this.server = s;
    this.zones = d;
    //console.log(this.interface_dns)
    //console.log(this.interface_dhcp)
  }

  add_zone() {
    this.zones.push({name: '', network: ''})
  }

  add_dhcp_range() {
    this.dhcp_ranges.push({start: '', end: '', lease_time: ''})
  }

  dnsmasq_fetch_cfggroups() {
    this.dnsmasqService.getGroups()
      .subscribe(o => this.cfgGroup = o);
  }
}
