import { Directive } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
import { Validate } from '../../assets/validate';

@Directive({
  selector: '[ipValidator]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: IpDirective,
    multi: true
  }]
})

export class IpDirective implements Validator {

  constructor() { }

  /**
   * Api
   * @param value 
   */
  check(value: String) : Boolean {
    return (Validate.is_ipv4(value) || Validate.is_ipv6(value))
  }

  /**
   * Validator
   * 
   * @param control 
   */
  validate(control: AbstractControl) : {[key: string]: any} | null {
    
    // allow empty values, check with required instead
    if (!control.value) {
      return null;
    }
    //console.log(this.check(control.value))
    if (!this.check(control.value))
      return { 'ipInvalid': true };
    
    //console.log("passed: " + control.value)
    return null; // return null if validation is passed.
  }
}