import { Directive } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
import { Validate } from '../../assets/validate';

@Directive({
  selector: '[networkValidator]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: NetworkDirective,
    multi: true
  }]
})
export class NetworkDirective implements Validator {

  constructor() { }

  /**
   * Api
   * @param value 
   */
  check(value: String) : Boolean {
    return Validate.is_network(value);
  }

  /**
   * Validator
   * 
   * @param control 
   */
  validate(control: AbstractControl) : {[key: string]: any} | null {
    // allow empty values, check with required instead
    if (!control.value) {
      return null;
    }
    
    if (!this.check(control.value)) {
      return { 'networkInvalid': true };
    }

    return null; // return null if validation is passed.
  }
}