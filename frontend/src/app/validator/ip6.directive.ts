import { Attribute, Directive } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
import { Validate } from '../../assets/validate';

@Directive({
  selector: '[ip6Validator]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: Ip6Directive,
    multi: true
  }]
})

export class Ip6Directive implements Validator {

  constructor() {}

  /**
   * Api
   * @param value 
   */
  check(value: String) : Boolean {
    return Validate.is_ipv6(value)
  }

  /**
   * Validator
   * 
   * @param control 
   */
  validate(control: AbstractControl) : {[key: string]: any} | null {
    console.log(`ip6Validator.value: ${control.value}`);
    
    // allow empty values, check with required instead
    if (!control.value) {
      return null;
    }
    //console.log(this.check(control.value))
    if (!this.check(control.value))
      return { 'ip6Invalid': true };
    
    //console.log("passed: " + control.value)
    return null; // return null if validation is passed.
  }
}
