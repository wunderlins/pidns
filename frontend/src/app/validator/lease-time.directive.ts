import { Directive } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
import { Validate } from '../../assets/validate';

@Directive({
  selector: '[leaseTimeValidator]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: LeaseTimeDirective,
    multi: true
  }]
})

export class LeaseTimeDirective implements Validator {

  constructor() { }

  /**
   * Api
   * @param value 
   */
  check(value: String) : Boolean {
    return Validate.is_lease_time(value);
  }

  /**
   * Validator
   * 
   * @param control 
   */
  validate(control: AbstractControl) : {[key: string]: any} | null {
    
    // allow empty values, check with required instead
    if (!control.value) {
      return null;
    }
    //console.log(this.check(control.value))
    if (!this.check(control.value))
      return { 'leaseTimeInvalid': true };
    
    //console.log("passed: " + control.value)
    return null; // return null if validation is passed.
  }
}