import { Attribute, Directive } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
import { Validate } from '../../assets/validate';

@Directive({
  selector: '[ip4Validator][ngModel]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: Ip4Directive,
    multi: true
  }]
})

export class Ip4Directive implements Validator {

  constructor() {}

  /**
   * Api
   * @param value 
   */
  check(value: String) : Boolean {
    console.log({type: "ip4Validator", value: value, valid: Validate.is_ipv4(value)})
    return Validate.is_ipv4(value)
  }

  /**
   * Validator
   * 
   * @param control 
   */
  validate(control: AbstractControl) : {[key: string]: any} | null {
    //console.log(`ip4Validator.value: ${control.value}`);
    //console.log(control)
    
    // allow empty values, check with required instead
    if (!control.value) {
      return null;
    }

    if (!this.check(control.value)) {
      return { 'ip4Invalid': true };
    }
    
    return null; // return null if validation is passed.
  }
}