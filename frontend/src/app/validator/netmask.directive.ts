import { Directive } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
import { Validate } from '../../assets/validate';

@Directive({
  selector: '[netmaskValidator]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: NetmaskDirective,
    multi: true
  }]
})
export class NetmaskDirective implements Validator {

  constructor() { }

  /**
   * Api
   * @param value 
   */
  check(value: String) : Boolean {
    console.log({type: "netmaskValidator", value: value, valid: Validate.is_netmask(value)})
    return Validate.is_netmask(value);
  }

  /**
   * Validator
   * 
   * @param control 
   */
  validate(control: AbstractControl) : {[key: string]: any} | null {
    // allow empty values, check with required instead
    if (!control.value) {
      return null;
    }
    
    if (!this.check(control.value)) {
      return { 'netmaskInvalid': true };
    }

    return null; // return null if validation is passed.
  }
}