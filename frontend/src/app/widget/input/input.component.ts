import { Component, OnInit, Input } from '@angular/core';
import { DnsmasqService } from '../../shared/dnsmasq.service';
import { CfgDirective } from '../../shared/cfg-directive.type';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {
  @Input() config: CfgDirective;
  @Input() name: string;
  @Input() value: string;

  constructor() { }

  ngOnInit(): void {
  }

}
