import { Component, Input, OnInit } from '@angular/core';
import { DnsmasqService } from 'src/app/shared';
import { Dnshost, RecordType } from '../../shared/dnshost.type';

class ToggleEvent extends Event {
  checked: Boolean;
}

@Component({
  selector: 'app-input-dnshost',
  templateUrl: './input-dnshost.component.html',
  styleUrls: ['./input-dnshost.component.css']
})
export class InputDnshostComponent implements OnInit {
  @Input() dnshost: Dnshost;
  typeValue: String;
  record_type: RecordType[];

  constructor(private dnsmasqService: DnsmasqService) {
    this.record_type = dnsmasqService.getRecordType();
  }

  ngOnInit(): void {
    //console.log("dnshost: " + this.dnshost)
  }

  onValuechange(o: Object): void {
    //console.log(o);
    this.dnshost[o["property"]] = o["value"];
  }

  update_add_dhcpaptr(event: ToggleEvent) {
    //console.log(event.checked)
    const key: String = "dhcp-host-a-ptr";
    const index = this.dnshost.options.indexOf(key, 0);
    if (event.checked) {
      if (index == -1)
        this.dnshost.options.push(key);
    } else {
      this.dnshost.options.splice(index, 1);
    }
  }

}
