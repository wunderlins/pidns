import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputDnshostComponent } from './input-dnshost.component';

describe('InputDnshostComponent', () => {
  let component: InputDnshostComponent;
  let fixture: ComponentFixture<InputDnshostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputDnshostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputDnshostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
