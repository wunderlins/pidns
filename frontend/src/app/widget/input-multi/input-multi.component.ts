import { Component, ContentChildren, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { 
  ControlContainer,
  ControlValueAccessor,
  NgForm
} from '@angular/forms';
import {
  FqdnDirective,
  HostnameDirective,
  IpDirective,
  Ip4Directive,
  Ip6Directive,
  LeaseTimeDirective,
  NetmaskDirective,
  NetworkDirective
} from '../../validator';

@Directive({selector: 'input'})
export class InputField {
  @Input() id!: string;
}
@Component({
  selector: 'app-input-multi',
  templateUrl: './input-multi.component.html',
  styleUrls: ['./input-multi.component.css'],
  viewProviders: [{ provide: ControlContainer, useExisting: NgForm}]
})

/**
 * Multiple text inputs
 * 
 * Manually adding validators, see:
 * https://stackoverflow.com/questions/38797414/in-angular-how-to-add-validator-to-formcontrol-after-control-is-created
 * https://stackoverflow.com/questions/60262229/access-ngform-of-parent-in-child
 */
export class InputMultiComponent implements OnInit, ControlValueAccessor {
  //@ViewChildren(InputField) inputFields!: QueryList<InputField>;

  _ip4Validator = false;
  _values: string[] = [];
  @Input()
  get values(): string[] {
    return this._values;
  }
  set values(value: string[]) {
    if (!value) {
      this._values = [];
      return;
    };
    console.log(`values.setter: ${value}`)
    this._values = value;
  }

  @Input() property: String;
  @Input() label: String;
  @Input() type: String = "text";
  //@Input() validator: Object;
  @Output() valuechange: EventEmitter<Object> = new EventEmitter<Object>();

  // validators, you may only provide one
  @Input() fqdnValidator;
  @Input() hostnameValidator;
  @Input() ipValidator;
  @Input()
  get ip4Validator(): boolean|string {
    return this._ip4Validator;
  }
  set ip4Validator(value: boolean|string) {
    console.log(`ip4Validator.setter: ${value}`)
    this._ip4Validator = value != null && value !== false && `${value}` !== 'false';
  }

  @Input() ip6Validator;
  @Input() leaseTimeValidator;
  @Input() netmaskValidator;
  @Input() networkValidator;
  
  // required is special, it checks if at least one non empty value is present
  @Input() required;

  constructor(private control : NgForm) {}

  /*
  // the method set in registerOnChange, it is just 
  // a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the form
  private propagateChange = (_: any) => { };

  // this is the initial value set to the component
  public writeValue(obj: any) {}

  // registers 'fn' that will be fired when changes are made
  // this is how we emit the changes back to the form
  public registerOnChange(fn: any) {
      this.propagateChange = fn;
  }
  // not used, used for touch input
  public registerOnTouched() { }
  validateValue(event: any) {
    this.propagateChange(this.values);
  }
  */

  ngOnInit(): void {
    // update value less attribute state
    if (this.fqdnValidator != null) this.fqdnValidator = true; else this.fqdnValidator = false;
    if (this.hostnameValidator != null) this.hostnameValidator = true; else this.hostnameValidator = false;
    if (this.ipValidator != null) this.ipValidator = true; else this.ipValidator = false;
    //if (this.ip4Validator != null) this.ip4Validator = true; else this.ip4Validator = false;
    if (this.ip6Validator != null) this.ip6Validator = true; else this.ip6Validator = false;
    if (this.leaseTimeValidator != null) this.leaseTimeValidator = true; else this.leaseTimeValidator = false;
    if (this.netmaskValidator != null) this.netmaskValidator = true; else this.netmaskValidator = false;
    if (this.networkValidator != null) this.networkValidator = true; else this.networkValidator = false;

    //if (this.values.length == 0)
    //  this.values = ['']

  }

  // observe changes to values
  
  private propagateChange = (_: any) => {
    this.onChange(_);
  };
  writeValue(obj: any): void {}
  registerOnTouched(fn: any): void {}
  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  onChange(_: any) {
    console.log(_)
    //console.log(this.control.form.controls.dnsserver_1)

    let ctrl = []
    for(let i=0; i<_.length; i++) {
      ctrl.push(this.control.form.controls[this.property + "_" + i])
    }

    console.log(ctrl)
  }

  ngAfterViewChecked(): void {
    //console.log(this.control.value);
  }

  ngAfterViewInit(): void {
    if (!this.values.length)
      return;
    
    console.table({
      fqdnValidator: this.fqdnValidator,
      hostnameValidator: this.hostnameValidator,
      ipValidator: this.ipValidator,
      ip4Validator: this._ip4Validator,
      ip6Validator: this.ip6Validator,
      leaseTimeValidator: this.leaseTimeValidator,
      netmaskValidator: this.netmaskValidator,
      networkValidator: this.networkValidator,
    });
    //console.log(this.inputFields);
  }

  /*
  updateValue(event: any) {
   this.values[event.target.i] = event.target.value;
    //console.log(this.values)
    this.valuechange.emit({
      property: this.property,
      value: this.values
    });

    this.propagateChange(this.values);
  }
  */

  identify(index, item) {
    return index;  
  }

  change(event: Event, index: number): void {
    const target = event.target as HTMLInputElement
    this.values[index] = target.value;
    this.propagateChange(this.values);
  }

  add(event: Event, index: number): void {
    //this.values.splice(index+1, 0, "");
    let v = this._values;
    v.splice(index+1, 0, "");
    this.values = v;
    this.propagateChange(this.values);
  }

  remove(event: Event, index: number): void {
    let v = this._values;
    v.splice(index, 1);
    this.values = v;
    this.propagateChange(this.values);
  }

  /*
  // validation
  public validate(c: FormControl) {
    console.log("validating: " + c.value)
    return null; // valid
  };
  */
}
