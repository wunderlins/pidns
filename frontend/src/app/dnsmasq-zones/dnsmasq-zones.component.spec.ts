import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DnsmasqZonesComponent } from './dnsmasq-zones.component';

describe('DnsmasqZonesComponent', () => {
  let component: DnsmasqZonesComponent;
  let fixture: ComponentFixture<DnsmasqZonesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DnsmasqZonesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DnsmasqZonesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
