import { hostViewClassName } from '@angular/compiler';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { Dnshost, RecordType, DnshostDhcp, DnshostSrv, DnshostCaa } from '../shared/dnshost.type';
import { DnsmasqService } from '../shared/dnsmasq.service';
import { InputDnshostComponent } from '../widget/input-dnshost/input-dnshost.component';
import { FqdnDirective, Ip4Directive } from '../validator';
@Component({
  selector: 'app-dnsmasq-zones',
  templateUrl: './dnsmasq-zones.component.html',
  styleUrls: ['./dnsmasq-zones.component.css']
})
export class DnsmasqZonesComponent implements OnInit {
  @Input() selected_host: Dnshost;
  empty_host: Dnshost;
  show_add_form: boolean = false;
  zones: String[];
  zones_ids: String[]; // css compatible ids of zone names
  zone: Dnshost[]; // list of hosts
  active: String;
  btn_color: String = "primary";
  icons: Map<String, String> = new Map<String, String>();
  new_host: Boolean = false;
  selected_index: Number = -1;

  constructor(private dnsmasqService: DnsmasqService) {
    let record_type = dnsmasqService.getRecordType();
    for (let e in record_type) {
      //console.log(record_type[e].value)
      this.icons.set(record_type[e].value, record_type[e].icon);
    }
    //console.log(this.icons)

    this.empty_host = {
      type: "dhcp-host",
      name: [],
      target: null,
      ipv4: null,
      ipv6: null,
      mac: [],
      alias: [],
      options: ["dhcp-host-a-ptr"],
  
      ttl: null,
      dhcp: {
        ignore: null,
        lease_time: null,
      },
      txt: [],
      mx: null,
      srv:  [null, null, null]  /*{
        port: null,
        priority: null,
        weight: null,
      }*/ ,
      caa: [null, null, null] /*{
        flags: null,
        tag: null,
        value: null,
      }*/ ,
    };
    this.selected_host = { ...this.empty_host};
  }

  ngOnInit(): void {
    //console.log("DnsmasqZonesComponent")
    //console.log(this.selected_host)

    this.dnsmasq_fetch_zones();
    /*
    this.selected_host.name  = ["localhost"];
    this.selected_host.type  = "cname";
    this.selected_host.ipv4  = "127.0.0.1";
    this.selected_host.ipv6  = "";
    this.selected_host.mac   = ["aa:bb:cc:00:11:22"];
    */
    
  }

  private zone_to_string(zone) {
    return "zone-" + zone.split(".").join("-")
  }

  dnsmasq_fetch_zones() {
    this.dnsmasqService.getZones()
      .subscribe(o => {
        this.zones = o;
        this.zones_ids = [];
        for (let e in this.zones) {
          this.zones_ids.push(this.zone_to_string(this.zones[e]))
        }
        //console.log(this.zones_ids)
        if (this.zones.length) {
          this.active = this.zones[0];
          this.dnsmasq_fetch_zone(this.zones[0])
        }
      });
  }

  dnsmasq_fetch_zone(zone: String) {
    this.dnsmasqService.getZone(zone)
      .subscribe(o => {
        this.zone = o;
        //console.log("Zone Data")
        //console.log(this.zone)
      });
  }

  save(): Dnshost {

    // we have to do some cleanup here, some fields 
    // are not used depending on the selected type

    // if this is a new host, copy it to zone<Dnshost>[]
    if (this.new_host) {
      var new_host = {...this.selected_host};
      this.selected_host = new_host;
      this.zone.push(this.selected_host)
      this.new_host = false;
      //console.log("new host");
      //console.log(new_host)
    }

    // only ttl for cname and host-record
    if (this.selected_host.type != 'cname' && 
        this.selected_host.type != 'host-record') {
      this.selected_host.ttl = null;
    }

    // dhcp options only for dhcp-host
    if (this.selected_host.type != 'dhcp-host') {
      this.selected_host.dhcp.ignore = null;
      this.selected_host.dhcp.lease_time = null;
    }

    // clear txt if not txt-record
    if (this.selected_host.type != 'txt-record') {
      this.selected_host.txt = [];
    }

    // empty mx if not mx-host
    if (this.selected_host.type != 'mx-host') {
      this.selected_host.mx = null;
    }

    // empty srv if not srv-host
    if (this.selected_host.type != 'srv-host') {
      this.selected_host.srv = [null, null, null]; // { port: null, priority: null, weight: null};
    }

    // empty caa if not caa-record
    if (this.selected_host.type != 'caa-record') {
      this.selected_host.caa = [null, null, null]; //  {flags: null, tag: null, value: null};
    }

    // default ignore is false
    if (!this.selected_host.dhcp.ignore) {
      this.selected_host.dhcp.ignore = false;
    }

    //console.log("Current Host Config:")
    //console.log(this.selected_host)
    this.show_add_form = false;

    this.dnsmasqService
      .updateZone(this.active, this.zone)
      .subscribe(data => {
      //console.log("Response from updateZone:");
      //console.log(data);
    });

    return this.selected_host;
  }

  set_host(newHost: Dnshost) {
    this.selected_host = newHost;
  }

  clear_host() {
    this.selected_host = this.empty_host;
  }

  active_zone(event: Event, zone: string) {
    this.active = zone;
    this.btn_color = "primary"
    //console.log("Zone selected: " + zone)
    this.dnsmasq_fetch_zone(this.active)
  }

  delete_entry(event: Event) {
    //const key: String = "dhcp-host-a-ptr";
    //this.zone.splice(this.selected_index, 1);
    //delete this.zone[this.selected_index];
    console.log(this.selected_index);
    var out: Dnshost[] = [];
    for (let i=0; i<this.zone.length; i++) {
      if (i != this.selected_index)
        out.push(this.zone[i])
    }
    this.zone = out;
    this.selected_index = -1;

    this.show_add_form = false;
    
    this.dnsmasqService
      .updateZone(this.active, this.zone)
      .subscribe(data => {
      console.log("Response from updateZone:");
      console.log(data);
    });
  }

  edit_host(host: Dnshost, index: Number) {
    this.selected_index = index;
    if (host == null) {
      this.selected_host = { ...this.empty_host};
      this.new_host = true;
    } else {
      this.selected_host = host;
      this.new_host = false;
    }
    this.show_add_form = true;
  }

  display_name(names: String[]): String[] {
    var out: String[] = [];
    for (let n in names) {
      //console.log(n)
      out.push(names[n].replace("." + this.active, ""))
    }
    return out;
  }
}
