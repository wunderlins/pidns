import { Component } from '@angular/core';
import { DnsmasqService } from './shared/dnsmasq.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'PiDNS';

  constructor(private dnsmasqService: DnsmasqService) {
    //dnsmasqService.getDirectives();
    //dnsmasqComponent.dnsmasq_fetch_cfgdirective();
  }
}
