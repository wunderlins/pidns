enum CfgDirectiveOpt {
    NoArg = 0,
    One,
    Dup
  }

export interface CfgDirective {
    type: number;
    key: string;
    value: string;
    description: string;
    groups: number[];
    exampleValue: string;
    options: String | null;
    opt: CfgDirectiveOpt;
}
