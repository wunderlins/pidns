import { TestBed } from '@angular/core/testing';

import { DnsmasqService } from './dnsmasq.service';

describe('DnsmasqService', () => {
  let service: DnsmasqService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DnsmasqService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
