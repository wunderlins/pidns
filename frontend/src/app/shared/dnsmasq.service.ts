import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of, pipe } from 'rxjs';
import { catchError, retry, map, tap, shareReplay } from 'rxjs/operators';

import { CfgDirective } from './cfg-directive.type';
import { CfgGroup } from './cfg-group.type';
import { Dnshost, RecordType } from './dnshost.type';

import { environment } from './../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

@Injectable()
export class DnsmasqService {
  cfgdirectiveObservable: Observable<CfgDirective[]>;
  cfgdirective: CfgDirective[] = [];
  config: Observable<CfgDirective[]>;
  cfgGroup: Observable<CfgGroup[]>;
  zones: Observable<String[]>;
  zone: Observable<Dnshost[]>;
  
  private dataUrl = environment.apiUrl;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient) { }

  getGroups(): Observable<CfgGroup[]> {
    return this.cfgGroup = this.http.get<CfgGroup[]>(this.dataUrl + "groups")
      .pipe(
        tap(_ => console.log('fetched Groups')),
        catchError(this.handleError<CfgGroup[]>('getGroups', []))
      );
  }

  getDirectives(): Observable<CfgDirective[]> {
    if (this.cfgdirective.length === 0) {
      console.log("CfgDirective: fetching ...")
      this.cfgdirectiveObservable = this.http.get<CfgDirective[]>(this.dataUrl + "cfgs")
      .pipe(
        shareReplay(1),
        tap(_ => console.log('fetched Directives')),
        catchError(this.handleError<CfgDirective[]>('getDirectives', [])),
      );
      this.cfgdirectiveObservable.subscribe(o => this.cfgdirective = o)
      return this.cfgdirectiveObservable;
    }
    console.log("CfgDirective: cached")
    return of(this.cfgdirective);
  }

  getZones(): Observable<String[]> {
    return this.zones = this.http.get<String[]>(this.dataUrl + "zones")
      .pipe(
        tap(_ => console.log('fetched Zones')),
        catchError(this.handleError<String[]>('getZones', []))
      );
  }

  getZone(zone: String): Observable<Dnshost[]> {
    return this.zone = this.http.get<Dnshost[]>(this.dataUrl + "zone/" + zone)
      .pipe(map((data: Dnshost[]) => {
          for (let e in data) {
            // adjust mapping to local data model
            //data[e].srv = {port: data[e].srv[0], priority: data[e].srv[1], weight: data[e].srv[2]}
            //data[e].caa = {flags: data[e].caa[0], tag: data[e].caa[1], value: data[e].caa[2]}
            //console.log(data[e]);
          }
          return data;
        }),
        tap(_ => console.log('fetched Zone')),
        catchError(this.handleError<Dnshost[]>('getZone', []))
      );
  }

  updateZone(zone: String, zone_data: Dnshost[]): Observable<String> {
    return this.http.put<String>(this.dataUrl + "zone/" + zone, zone_data, httpOptions)
      .pipe(
        catchError(this.handleError('updateZone', zone))
      );
  }

  getRecordType(): RecordType[] {
    return [
      {value: "dhcp-host",    viewValue: "DHCP Host (Static DHCP + DNS)", icon: "lock"},
      {value: "host-record",  viewValue: "Host (A/AAAA/PTR)", icon: "lock_open"},
      {value: "cname",        viewValue: "Alias (cname)", icon: "content_copy"},
      {value: "ptr-record",   viewValue: "Pointer (PTR)", icon: "double_arrow"},
      {value: "srv-host",     viewValue: "Server (SRV)", icon: "dns"},
      {value: "txt-record",   viewValue: "Text (TXT)", icon: "description"},
      {value: "mx-host",      viewValue: "Mailrelay (MX)", icon: "mail"},
      // {value: "naptr-record", viewValue: "Name Authority Pointer (NAPTR)", icon: ""},
      {value: "caa-record",   viewValue: "Certification Authority Auth. (CAA)", icon: "security"},
    ];
  
  }

  getConfig(): Observable<CfgDirective[]> {
    return this.config = this.http.get<CfgDirective[]>(this.dataUrl + "config")
      .pipe(
        tap(_ => console.log('fetched Config')),
        catchError(this.handleError<CfgDirective[]>('getConfig', []))
      );
  }

  getDirective(name: string): Observable<CfgDirective> {
    const url = `${this.dataUrl}/cfg/${name}`;
    return this.http.get<CfgDirective>(url).pipe(
      tap(_ => console.log(`fetched Directive name=${name}`)),
      catchError(this.handleError<CfgDirective>(`getDirective name=${name}`))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
