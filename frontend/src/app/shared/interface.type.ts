import { Ip } from './ip.type';

export interface Interface {
    index: number;
    iface: string;
    hw_addr: string;
    default_gw: string;
    ip_addr4: Ip[];
    ip_addr6: Ip[];
    state: string;
    linkmode: number;
}
