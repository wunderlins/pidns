import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, interval } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { Interface } from './interface.type';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InterfacesService {
  interfaces: Observable<Interface[]>;
  
  private dataUrl = environment.apiUrl;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient) { }

  getInterfaces(): Observable<Interface[]> {
    return this.interfaces = this.http.get<Interface[]>(this.dataUrl + "interfaces")
      .pipe(
        tap(_ => console.log('fetched Interfaces')),
        catchError(this.handleError<Interface[]>('getInterfaces', []))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
