/*
model_dnshost = api.model('Dnshost', {
    'type': fields.String,
    'name': fields.List(fields.String),
    'target': fields.String,
    'ipv4': fields.String,
    'ipv6': fields.String,
    'mac': fields.List(fields.String),
    'alias': fields.List(fields.String),

    'ttl': fields.Integer,
    'dhcp': fields.List(fields.String),
    'txt': fields.List(fields.String),
    'mx': fields.String,
    'srv': fields.List(fields.Integer),
    'caa': fields.List(fields.String),
})
*/

export interface RecordType {
    value: String;
    viewValue: String;
    icon: String;
}

export interface DnshostDhcp {
    ignore: Boolean | null;
    lease_time: String | null;
}

export interface DnshostSrv {
    port: Number | null;
    priority: Number | null;
    weight: Number | null;
}

export interface DnshostCaa {
    flags: String | null;
    tag: String | null;
    value: String | null;
}

export interface Dnshost {
    type: String;
    name: String[] | null;
    target: String | null;
    ipv4: String | null;
    ipv6: String | null;
    mac: String[] | null;
    alias: String[] | null;
    options: String[] | null;

    ttl: Number | null;
    dhcp: DnshostDhcp | null;
    txt: String[] | null;
    mx: String | null;
    srv: number[]; // DnshostSrv | null;
    caa: String[]; // DnshostCaa | null
}
