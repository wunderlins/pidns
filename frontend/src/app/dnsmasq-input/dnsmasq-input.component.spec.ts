import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DnsmasqInputComponent } from './dnsmasq-input.component';

describe('DnsmasqInputComponent', () => {
  let component: DnsmasqInputComponent;
  let fixture: ComponentFixture<DnsmasqInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DnsmasqInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DnsmasqInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
