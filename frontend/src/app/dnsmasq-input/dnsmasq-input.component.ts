import { Component, OnInit, Input } from '@angular/core';
import { CfgDirective } from '../shared/cfg-directive.type';
import {Directive, HostListener} from "@angular/core";
import { DnsmasqService } from '../shared/dnsmasq.service';
import { CfgGroup } from '../shared/cfg-group.type';
import { ViewChild } from '@angular/core';
import {MatExpansionPanel} from '@angular/material/expansion';

@Component({
  selector: 'app-dnsmasq-input',
  templateUrl: './dnsmasq-input.component.html',
  styleUrls: ['./dnsmasq-input.component.css']
})
export class DnsmasqInputComponent implements OnInit {
  @Input() cfgDirective: CfgDirective;
  @Input() configs: CfgDirective[];
  @ViewChild('accordion') accordion: MatExpansionPanel;
  value: string = "";
  panelOpenState = false;

  constructor() { }

  public toggle(event: any): void {
    console.log(this.accordion)
    this.accordion.toggle()
  }

  ngOnInit(): void {
    //console.log(this.dnsmasqService.config)
    //console.log(this.configs)
    for (let e in this.configs) {
      if (this.configs[e].key == this.cfgDirective.key)
        this.value = this.configs[e].value;
    }
  }

  ngAfterViewInit(): void {
  }

}
