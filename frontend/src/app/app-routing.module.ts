import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DnsmasqComponent } from './dnsmasq/dnsmasq.component'
import { DnsmasqConfigComponent } from './dnsmasq-config/dnsmasq-config.component'
import { DnsmasqZonesComponent } from './dnsmasq-zones/dnsmasq-zones.component'
import { DashboardComponent } from './dashboard/dashboard.component'
import { ErrorComponent } from './error/error.component';

const routes: Routes = [
  { path: 'dnsmasq', component: DnsmasqComponent },
  { path: 'dnsmasq-config', component: DnsmasqConfigComponent },
  { path: 'dnsmasq-zones', component: DnsmasqZonesComponent },
  
  { path: '', component: DashboardComponent },
  { path: '**', component: ErrorComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
