import { Component, OnInit } from '@angular/core';
import { InterfacesService } from '../shared/interfaces.service';
import { Interface } from '../shared/interface.type';
import { Observable, of, interval, Subscription } from 'rxjs';
import { environment } from './../../environments/environment';

@Component({
  selector: 'app-interfaces',
  templateUrl: './interfaces.component.html',
  styleUrls: ['./interfaces.component.css']
})
/**
 * FIXME: the interval timer is not being stopped when user leaves view.
 *        a new one is created when the user comes back, 
 *        duplicating the timer.
 */
export class InterfacesComponent implements OnInit {
  refreshRate = environment.interfaces.refresh_interval_ms;
  private interval: any = null;
  private subscription: Subscription = new Subscription();
  
  interfaces: Interface[];
  displayedColumns: string[] = ['linkmode', 'index', 'iface', 'details' ];
  constructor(private interfacesService: InterfacesService) { 
  }

  ngOnInit(): void {
    this.interfaces_fetch()
    if (this.refreshRate > 0 && !this.interval) {
      this.interval = interval(this.refreshRate).subscribe((value: number) => {
        this.interfaces_fetch()
      });
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    clearInterval(this.interval);
    this.interval = null;
  }

  interfaces_fetch() {
    this.subscription.add(
      this.interfacesService.getInterfaces()
        .subscribe(o => this.interfaces = o)
    );
  }
}
