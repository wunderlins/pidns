import { NgModule, Component, ViewChild } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatChipsModule } from '@angular/material/chips';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatBadgeModule } from '@angular/material/badge';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { AppComponent } from './app.component';
import { DnsmasqComponent } from './dnsmasq/dnsmasq.component';
import { DnsmasqInputComponent } from './dnsmasq-input/dnsmasq-input.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ErrorComponent } from './error/error.component';
import { InterfacesComponent } from './interfaces/interfaces.component';
import { InputComponent } from './widget/input/input.component';
import { DnsmasqConfigComponent } from './dnsmasq-config/dnsmasq-config.component';
import { DnsmasqZonesComponent } from './dnsmasq-zones/dnsmasq-zones.component';
import { InputDnshostComponent } from './widget/input-dnshost/input-dnshost.component';
import { InputMultiComponent } from './widget/input-multi/input-multi.component';
import { NetworkDirective } from './validator/network.directive';
import { NetmaskDirective } from './validator/netmask.directive';
import { IpDirective } from './validator/ip.directive';
import { Ip4Directive } from './validator/ip4.directive';
import { Ip6Directive } from './validator/ip6.directive';
import { LeaseTimeDirective } from './validator/lease-time.directive';
import { HostnameDirective } from './validator/hostname.directive';
import { FqdnDirective } from './validator/fqdn.directive';

@NgModule({
  declarations: [
    AppComponent,
    DnsmasqComponent,
    DnsmasqInputComponent,
    DashboardComponent,
    ErrorComponent,
    InterfacesComponent,
    InputComponent,
    DnsmasqConfigComponent,
    DnsmasqZonesComponent,
    InputDnshostComponent,
    InputMultiComponent,
    NetworkDirective,
    NetmaskDirective,
    IpDirective,
    Ip4Directive,
    Ip6Directive,
    LeaseTimeDirective,
    HostnameDirective,
    FqdnDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    MatInputModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatIconModule,
    MatExpansionModule,
    MatButtonModule,
    MatDividerModule,
    MatChipsModule,
    MatMenuModule,
    MatTableModule,
    MatCardModule,
    MatSelectModule,
    FormsModule,
    MatButtonToggleModule,
    MatBadgeModule,
    MatSlideToggleModule,
    MatCheckboxModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
}
