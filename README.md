# PiDNS

PiDNS aims to be a simple to use DNS/DHCP web interface for home use
targeted for the Raspberry PI. Development takes place on Debian so
the package will probably be usabale on any Debian derived system if
not generally on Linux.

## DNS Blacklist

    curl -SLso dnsmasq.blacklist.txt https://raw.githubusercontent.com/notracking/hosts-blocklists/master/dnsmasq/dnsmasq.blacklist.txt


## Roadmap

- [ ] Host management
    - [x] config parser
    - [x] List zones and hosts
    - [x] add/save host
    - [x] remove host
    - [x] add `host-record` to every dhcp-host
        - [x] parser must ignore additional `host-record` field for dhcp-host
    - [ ] input validation
    - [x] `dhcp-host` may only have one `name` property, maybe others ?
        - [x] check on backend, use `name[0]`
        - [x] allow multiple in frontend only if backend allows
    - [ ] Better List layout for desktop screens
    - [ ] move host definitions to `--dhcp-hostsdir`, this wil ltrigegr automatic reload -> deleted and changed entries are not reloaded!
    - [ ] Fix zone selection
        - [X] select default zone
        - [X] zone selection as menu
        - [ ] zone selection by adding/editing host (at least show selected host)
- [ ] Dashboard / Service Management
    - [x] list of interfaces
    - [ ] General DNS Configuration
        - [ ] service management
        - [x] main dns config interface
        - [ ] main dns config rest interface
        - [x] example `dnsmasq.conf`
        - [ ] dns blacklists
    - [ ] General DHCP Configuration
        - [ ] service management
        - [ ] general DHCP configurations
        - [ ] ui for seting options
    - [ ] General NTP Configuration
- [ ] add input validation for
    - [ ] hostname
    - [ ] hostname_fqdn
    - [ ] ip4_addr
    - [ ] ip6_addr
    - [ ] ip_addr (v4 or v6)
    - [ ] netmask (int)
    - [ ] lease_time
    - [ ] mac_addr
    - [ ] TTL (int)
- [ ] Robust error handling
- [ ] List of Leases
- [ ] Authentication
    - [ ] PAM
- [ ] SSL
- [ ] Release
    - [x] refactor directory structure `/usr/bin` 
          `/usr/lib/pidns` `/usr/share/pidns` `/etc` `/var` 
    - [ ] disable debugging everywhere
    - [ ] uwsgi integration
        - [x] start/stop script
        - [ ] serve static asses from `uWSGI`
    - [ ] systemd
        - [ ] pidns-web
        - [ ] pidns-dnsmasq
    - [ ] disable swagger interface
    - [ ] .deb package
    - [ ] Optional: .sh install script for other platforms ?

## Code generation

To support newer versions of dnsmasq you might have to generate new
configuration directives. Update `tools/dnsmasq_args.c` from
the original dnsmasq source [src/options.c](http://thekelleys.org.uk/gitweb/?p=dnsmasq.git;a=blob;f=src/option.c;h=dbe5f9054b8066ae6d62400e016d03ade127a980;hb=HEAD) and run `make`.

## Ressources

- [DNSMasq Manpage](http://www.thekelleys.org.uk/dnsmasq/docs/dnsmasq-man.html)
- [DNSMasq Basic Config](https://wiki.freebsd.org/BernardSpil/DHCP_DNS)
- [Angular Material Components](https://material.angular.io/components/categories)
- [Angular Material Icons](https://material.io/resources/icons/?style=baseline)
- [pyroute 2 IPDB](https://docs.pyroute2.org/ipdb.html)
- [Flask](https://flask.palletsprojects.com/en/1.1.x/)
