#!/usr/bin/env python3
#

import re

"""
  // http://www.thekelleys.org.uk/dnsmasq/docs/dnsmasq-man.html
  record_type = [
    // --dhcp-host=
    //     [<hwaddr>]
    //     [,id:<client_id>|*]
    //     [,set:<tag>][tag:<tag>]
    //     [,<ipaddr>]
    //     [,<hostname>]
    //     [,<lease_time>]
    //     [,ignore]
    {value: "dhcp-host",    viewValue: "DHCP Host (Static DHCP + DNS)"},
    
    // --host-record=<name>[,<name>....],[<IPv4-address>],[<IPv6-address>][,<TTL>]
    {value: "host-record",  viewValue: "Host (A/AAAA/PTR)"},
    
    // --cname=<cname>,[<cname>,]<target>[,<TTL>]
    {value: "cname",        viewValue: "Alias (cname)"},

    // --ptr-record=<name>[,<target>]
    {value: "ptr-record",   viewValue: "Pointer (PTR)"},

    // --srv-host=<_service>.<_prot>.[<domain>],[<target>[,<port>[,<priority>[,<weight>]]]]
    {value: "srv-host",     viewValue: "Server (SRV)"},

    // --txt-record=<name>[[,<text>],<text>]
    {value: "txt-record",   viewValue: "Text (TXT)"},

    // --mx-host=<mx name>[[,<hostname>],<preference>]
    {value: "mx-host",      viewValue: "Mailrelay (MX)"},

    // --naptr-record=<name>,<order>,<preference>,<flags>,<service>,<regexp>[,<replacement>]
    {value: "naptr-record", viewValue: "Name Authority Pointer (NAPTR)"},

    // --caa-record=<name>,<flags>,<tag>,<value>
    {value: "caa-record",   viewValue: "Certification Authority Auth. (CAA)"},
  ];
"""

debug = True

if debug:
    import os, sys
    from pprint import pprint
    sys.path.insert(0, os.path.realpath("../"))

import re
from lib.pidns.CfgDirective import *

multiple_names = ["cname", "host-record"]

class Dnshost:
    type       = None
    name       = []
    target     = None
    ipv4       = None
    ipv6       = None # FIXME: multiple v6 addresses allowd for dhcp-host
    mac        = []
    alias      = []

    # extra pidns options
    options    = []

    # cname, host-record
    ttl        = None # int

    # dhcp-host
    ignore     = False
    lease_time = None

    # txt-record
    txt        = []

    # mx-host
    preference = None # int

    # srv-host
    port       = None # int
    priority   = None # int
    weight     = None # int

    # caa-record
    flags      = None
    tag        = None
    value      = None

    record_type = {
        "dhcp-host"    : "DHCP Host (Static DHCP + DNS)",
        "host-record"  : "Host (A/AAAA/PTR)",
        "cname"        : "Alias (cname)",
        "ptr-record"   : "Pointer (PTR)",
        "srv-host"     : "Server (SRV)",
        "txt-record"   : "Text (TXT)",
        "mx-host"      : "Mailrelay (MX)",
        "caa-record"   : "Certification Authority Auth. (CAA)",
    }
    #    "naptr-record" : "Name Authority Pointer (NAPTR)",

    def __init__(self, cfgdirective=None, json=None):
        if cfgdirective == None and json == None:
            raise Exception("Must provide either cfgdirective or json")
        
        if json != None:
            self.from_json(json)
        else:
            if cfgdirective.key not in self.record_type.keys():
                raise NameError(cfgdirective.key)
            
            self.ignore = False
            self.mac   = []
            self.alias = []
            self.name  = []
            self.txt   = []
            self.type  = cfgdirective.key
            self.from_string(cfgdirective.key, cfgdirective.value)
            self.options  = cfgdirective.options
    
    def __repr__(self):
        return "<Dnshost: " + str(self.to_json()) + ">"
        
        out = "<Dnshost: "
        out += "type=" + str(self.type) + ", "
        out += "name=" + str(self.name) + ", "
        out += "target=" + str(self.target) + ", "
        out += "ipv4=" + str(self.ipv4) + ", "
        out += "ipv6=" + str(self.ipv6) + ", "
        out += "mac=" + str(self.mac) + ", "
        out += "ignore=" + str(self.ignore) + ", "
        out += "lease_time=" + str(self.lease_time) + ", "
        out += "ttl=" + str(self.ttl) + ", "
        out += "txt=" + str(self.txt) + ", "
        out += "preference=" + str(self.preference) + ", "
        out += "srv=" + str([self.port, self.priority, self.weight]) + ", "
        out += "caa=" + str([self.flags, self.tag, self.value]) + ">"
        return out

    def list_to_string(self, l):
        # remove empty entries
        l = [x for x in l if x != None and x.strip()]
        # print(l)

        # join by comma
        return ",".join(l)
    
    def append_str(self, buffer, append):
        if buffer and buffer[-1] != ",":
            buffer += ","
        return buffer + append

    def to_cfg_name(self, out):
        if len(self.name):
            if self.type in multiple_names:
                #print(self.name)
                out = self.append_str(out, self.list_to_string(self.name))
            else:
                out = self.append_str(out, self.name[0])
        else:
            raise NameError("At least one name required for host-record")
        
        return out

    def to_cfg_ip(self, out):
        #print(self.ipv4)
        if self.ipv4:
            out = self.append_str(out, self.ipv4)
        
        #print(self.ipv6)
        if self.ipv6:
            out = self.append_str(out, self.ipv6)
        
        return out

    def quote(v):
        if " " not in v and '"' not in v and "\\" not in v:
            return v
        
        if v and v[0] != '"' and v[0] != "'":
            v = v.replace("\\", "\\\\")
            v = v.replace("\"", "\\\"")
            return '"' + v + '"'

        return v

    def to_cfg(self):
        """ generate config directive
        """

        out = ""

        if self.type == "dhcp-host":
            """ generate dhcp-host String 
            record_type = [
                // --dhcp-host=
                //     [<hwaddr>]
                //     [,id:<client_id>|*]
                //     [,set:<tag>][tag:<tag>]
                //     [,<ipaddr>]
                //     [,<hostname>]
                //     [,<lease_time>]
                //     [,ignore]
            """
            if self.mac:
                #print(str(self.mac))
                out = self.append_str(out, self.list_to_string(self.mac))
            
            #print(out)
            out = self.to_cfg_ip(out)
            #print(out)
            
            out = self.to_cfg_name(out)

            if self.lease_time:
                out = self.append_str(out, self.lease_time)

            if self.ignore:
                if self.ignore:
                    out = self.append_str(out, "ignore")
            
            return self.type + "=" + out

        if self.type == "host-record":
            """ generate host-record string 
                // --host-record=<name>[,<name>....],[<IPv4-address>],[<IPv6-address>][,<TTL>]
                {value: "host-record",  viewValue: "Host (A/AAAA/PTR)"},
            """

            out = self.to_cfg_name(out)
            out = self.to_cfg_ip(out)
            
            if self.ttl:
                out = self.append_str(out, str(self.ttl))
            
            return self.type + "=" + out

        if self.type == "cname":
            """generate cname config String 
                // --cname=<cname>,[<cname>,]<target>[,<TTL>]
                {value: "cname",        viewValue: "Alias (cname)"},
            """

            out = self.to_cfg_name(out)

            if self.target:
                out = self.append_str(out, self.target)
            
            if self.ttl:
                out = self.append_str(out, str(self.ttl))
            
            return self.type + "=" + out

        if self.type == "ptr-record":
            """ generate ptr-record config string
                // --ptr-record=<name>[,<target>]
                {value: "ptr-record",   viewValue: "Pointer (PTR)"},
            """
            out = self.to_cfg_name(out)

            if self.target:
                out = self.append_str(out, Dnshost.quote(self.target))
            
            return self.type + "=" + out

        if self.type == "txt-record":
            """ generate txt-record config string
                // --txt-record=<name>[[,<text>],<text>]
                {value: "txt-record",   viewValue: "Text (TXT)"},
            """
            # FIXME: txt-record=example.com,\"v
            out = self.to_cfg_name(out)
            if len(self.txt):
                # quote values
                vals = []
                for v in self.txt:
                    vals.append(Dnshost.quote(v))

                out = self.append_str(out, self.list_to_string(vals))

            return self.type + "=" + out

        if self.type == "mx-host":
            """ generate mx-host config string
                // --mx-host=<mx name>[[,<hostname>],<preference>]
                {value: "mx-host",      viewValue: "Mailrelay (MX)"},
            """
            out = self.to_cfg_name(out)

            if self.target:
                out = self.append_str(out, self.target)
            
            if self.preference:
                out = self.append_str(out, str(self.preference))

            return self.type + "=" + out

        if self.type == "srv-host":
            """ generate srv-host config string
                // --srv-host=<_service>.<_prot>.[<domain>],[<target>[,<port>[,<priority>[,<weight>]]]]
                {value: "srv-host",     viewValue: "Server (SRV)"},
            """

            out = self.to_cfg_name(out)

            if self.target:
                out = self.append_str(out, self.target)

                if self.port:
                    out = self.append_str(out, str(self.port))

                    if self.priority:
                        out = self.append_str(out, str(self.priority))

                        if self.weight:
                            out = self.append_str(out, str(self.weight))

            return self.type + "=" + out

        #if type == "naptr-record":
        #    self.parse_naptr_record(parts)

        if self.type == "caa-record":
            """ generate caa-record config string
                // --caa-record=<name>,<flags>,<tag>,<value>
                {value: "caa-record",   viewValue: "Certification Authority Auth. (CAA)"},

            """
            out = self.to_cfg_name(out)

            if self.flags:
                out = self.append_str(out, str(self.flags))

            if self.tag:
                out = self.append_str(out, str(self.tag))

            if self.value:
                out = self.append_str(out, str(self.value))
        
            return self.type + "=" + out

        return None
    
    def to_json(self):
        ret = {
            "type": self.type,
            "name": self.name,
            "target": self.target,
            "ipv4": self.ipv4,
            "ipv6": self.ipv6,
            "mac": self.mac,
            "alias": self.alias,
            "options": self.options,

            "ttl": self.ttl,
            "dhcp": { "ignore": self.ignore, "lease_time": self.lease_time},
            "txt": self.txt,
            "mx": self.preference,
            "srv": [self.port, self.priority, self.weight],
            "caa": [self.flags, self.tag, self.value]
        }

        return ret
    
    def from_json(self, json):
        self.type       = json["type"]
        self.name       = json["name"]
        self.target     = json["target"]
        self.ipv4       = json["ipv4"]
        self.ipv6       = json["ipv6"]
        self.mac        = json["mac"]
        self.alias      = json["alias"]

        # special pidns options
        self.options    = json["options"]

        # cname, host-record
        self.ttl        = json["ttl"] # int

        # dhcp-host
        self.ignore     = json["dhcp"]["ignore"]
        self.lease_time = json["dhcp"]["lease_time"]

        # txt-record
        self.txt        = json["txt"]

        # mx-host
        self.preference = json["mx"]  # int

        # srv-host
        self.port       = json["srv"][0] # int
        self.priority   = json["srv"][1] # int
        self.weight     = json["srv"][2] # int

        # caa-record
        self.flags      = json["caa"][0]
        self.tag        = json["caa"][1]
        self.value      = json["caa"][2]

        if self.ttl != None: self.ttl = int(self.ttl)
        if self.preference != None: self.preference = int(self.preference)

        if self.port != None: self.port = int(self.port)
        if self.priority != None: self.priority = int(self.priority)
        if self.weight != None: self.weight = int(self.weight)


    def line_split(line, delim=",", quotechar=["'", '"']):
        """ split line into list, don't split inside quotes """
        out = []
        l = len(line)
        buffer = ""
        in_quotes = False

        for i in range(0, l):
            char = line[i]

            if char in quotechar:
                # ignore backslash escaped quotes
                if i > 0 and line[i-1] == "\\":
                    pass
                else:
                    if in_quotes:
                        in_quotes = False
                        continue
                    else:
                        in_quotes = True
                        continue
        
            if char == delim:
                if in_quotes:
                    buffer += char
                    continue
                out.append(buffer)
                buffer = ""
            else:
                buffer += (char)
        
        if buffer:
            out.append(buffer)    
        
        #print(out)
        return out

    
    def from_string(self, type, value):
        parts = []
        #parts = value.split(",")

        #print(value, len(value))
        if value:
            #parts = shlex.split(value, comments=False)
            parts = Dnshost.line_split(value)
        
        #print(str(parts))
        #return

        if type == "dhcp-host":
            self.parse_dhcp_host(parts)

        if type == "host-record":
            self.parse_host_record(parts)

        if type == "cname":
            self.parse_cname(parts)

        if type == "ptr-record":
            self.parse_ptr_record(parts)

        if type == "txt-record":
            self.parse_txt_record(parts)

        if type == "mx-host":
            self.parse_mx_host(parts)

        if type == "srv-host":
            self.parse_srv_host(parts)

        #if type == "naptr-record":
        #    self.parse_naptr_record(parts)

        if type == "caa-record":
            self.parse_caa_record(parts)

    def parse_dhcp_host(self, parts):

        # parse part, try to handle the most obvious cases first
        while True:
            if len(parts) == 0: return
            v = parts.pop(0)
            v = v.strip()
            
            if len(v) > 4 and (v[0:3] == "id:" or v[0:4] in ["set:", "tag:"]):
                # TODO: unhandled features
                continue

            if v == "ignore":
                #print([self.name, self.mac[0]])
                self.ignore = True
                continue

            if v == "infinite":
                self.lease_time = v
                continue

            if Dnshost.is_mac(v):
                #print("mac " + v)
                self.mac.append(v)
                continue

            if Dnshost.is_lease_time(v):
                #print("lease_time " + v)
                self.lease_time = v
                continue
            
            if Dnshost.is_ipv4(v):
                #print("ipv4 " + v)
                self.ipv4 = v
                continue

            if Dnshost.is_ipv6(v):
                #print("ipv6 " + v)
                self.ipv6 = v
                continue
        
            if Dnshost.is_fqdn(v):
                #print("name " + v)
                self.name.append(v)
                continue
        

    def parse_host_record(self, parts):
        # parse part, try to handle the most obvious cases first
        while True:
            if len(parts) == 0: return
            v = parts.pop(0)
            v = v.strip()

            # test for ttl, is int value
            if v.isnumeric():
                self.ttl = int(v)
                continue
            
            # test for ipv4 address
            if Dnshost.is_ipv4(v):
                self.ipv4 = v
                continue

            # test for ipv6 address
            if Dnshost.is_ipv6(v):
                self.ipv6 = v
                continue
        
            # test for fqdn
            if Dnshost.is_fqdn(v):
                self.name.append(v)
                continue

    def parse_cname(self, parts):
        # parse part, try to handle the most obvious cases first
        while True:
            if len(parts) == 0: 
                break
            
            v = parts.pop(0)
            v = v.strip()

            # test for ttl, is int value
            if v.isnumeric():
                self.ttl = int(v)
                continue
            
            # test for fqdn
            if Dnshost.is_fqdn(v):
                self.name.append(v)
                continue
        
        # the last host name is the target
        self.target = self.name.pop()

    def parse_ptr_record(self, parts):
        self.name.append(parts[0])
        if len(parts) > 1:
            v = parts[1].strip()
            if v[0] == '"' or  v[0] == "'":
                v = v[1:-1] # remove quotes
            self.target = v

    def parse_txt_record(self, parts):
        self.name.append(parts[0])
        
        if len(parts) > 1:
            v = parts[1].strip()
            if v[0] == '"' or  v[0] == "'":
                v = v[1:-1] # remove quotes
            self.txt.append(v)

        if len(parts) > 2:
            v = parts[2].strip()
            if v[0] == '"' or  v[0] == "'":
                v = v[1:-1] # remove quotes
            self.txt.append(v)

    def parse_mx_host(self, parts):
        self.name.append(parts[0])
        
        if len(parts) > 1:
            v = parts[1].strip()
            if v[0] == '"' or  v[0] == "'":
                v = v[1:-1] # remove quotes
            self.target = v

        if len(parts) > 2:
            v = parts[2].strip()
            if v[0] == '"' or  v[0] == "'":
                v = v[1:-1] # remove quotes
            self.preference = int(v)

    def parse_srv_host(self, parts):
        self.name.append(parts.pop(0))

        l = len(parts)

        # target
        if l > 0:
            self.target = parts[0]

        # port
        if l > 1:
            self.port = int(parts[1])

        # priority
        if l > 2:
            self.priority = int(parts[2])

        # priority
        if l > 3:
            self.weight = int(parts[3])


    #def parse_naptr_record(self, parts):
    #    pass

    def parse_caa_record(self, parts):
        self.name.append(parts.pop(0))
        self.flags = parts.pop(0)
        self.tag = parts.pop(0)
        self.value = parts.pop(0)

    def to_string(self):
        pass
    
    def is_ipv4(value):
        parts = value.split(".")
        if len(parts) != 4:
            return False
        
        for e in parts:
            i = -1
            try:
                i = int(e)
            except:
                return False
            
            if i < 0 or i > 255:
                return False
        
        return True
        
    def is_ipv6(value):
        if ":" not in value:
            return False
        
        parts = value.split(":")
        if len(parts) > 8:
            return False
        
        for e in parts:
            if e == "":
                continue
            
            if len(e) > 4:
                return False
            
            i = -1
            try:
                i = int("0x"+e, 0)
            except:
                return False
            
            if i > 65535:
                return False
            
        return True

    def is_lease_time(value):
        if value.lower() == "infinite":
            return True

        if value.isnumeric():
            return True

        # must be numeric (seconds) or suffixed by
        # - [sS] Seconds
        # - [mM] Minutes
        # - [hH] Hours
        if value[0:-1].isnumeric() and value[-1:] in "sSmMhHdDwW":
            return True

        return False

    def is_mac(value):
        """ check if string is a valid mac address 
        
        in some definitions allow wildcards in mac addresses like
        11:22:33:*, take this into account
        
        """
        parts = value.split(":")
        asterisks = False

        if len(parts) > 6:
            return False
        
        for e in parts:
            if e[0] == "*" or (len(e) > 1 and e[1] == "*"):
                #print(e)
                asterisks = True
                continue

            if len(e) != 2:
                return False

            if e[0] not in "0123456789abcdefABCDEF":
                return False

            if len(e) > 1 and e[1] not in "0123456789abcdefABCDEF":
                return False

        if asterisks == False and len(parts) != 6:
            return False
        
        return True

    # https://stackoverflow.com/questions/2532053/validate-a-hostname-string
    def is_fqdn(value):
        if value.endswith('.'):
            value = value[:-1]
        if len(value) < 1 or len(value) > 253:
            return False
        ldh_re = re.compile('^[a-z0-9]([a-z0-9-]{0,61}[a-z0-9])?$',
                            re.IGNORECASE)
        ret = all(ldh_re.match(x) for x in value.split('.'))
        #pprint(ret)
        return ret

if __name__ == "__main__":
    configs = [
        # cname
        Dnshost(CfgDirective(CfgType.STRING, key="cname", value="bertand,bert")),
        Dnshost(CfgDirective(CfgType.STRING, key="cname", value="bertand,bertrand2,bert")),

        # dhcp-host
        Dnshost(CfgDirective(CfgType.STRING, key="dhcp-host", value="11:22:33:44:55:66,fred,192.168.0.60,45m")),
        Dnshost(CfgDirective(CfgType.STRING, key="dhcp-host", value="11:22:33:44:55:66,12:34:56:78:90:12,192.168.0.60")),
        Dnshost(CfgDirective(CfgType.STRING, key="dhcp-host", value="bert,192.168.0.70,infinite")),
        Dnshost(CfgDirective(CfgType.STRING, key="dhcp-host", value="11:22:33:44:55:66,fred,2001:db8::8a2e:370:7334,45m")),
        Dnshost(CfgDirective(CfgType.STRING, key="dhcp-host", value="11:22:33:44:*,ignore")),
        Dnshost(CfgDirective(CfgType.STRING, key="dhcp-host", value="11:22:33:44:*,test.intra.wunderlin.net,ignore")),

        # host-record
        Dnshost(CfgDirective(CfgType.STRING, key="host-record", value="laptop,laptop.thekelleys.org,192.168.0.1,1234::100")),
        Dnshost(CfgDirective(CfgType.STRING, key="host-record", value="laptop,laptop.thekelleys.org,192.168.0.1,1234::100,800")),
        
        # ptr-record
        Dnshost(CfgDirective(CfgType.STRING, key="ptr-record", value="_http._tcp.dns-sd-services")),
        Dnshost(CfgDirective(CfgType.STRING, key="ptr-record", value="_http._tcp.dns-sd-services,\"New Employee Page._http._tcp.dns-sd-services\"")),
        
        # txt-record
        Dnshost(CfgDirective(CfgType.STRING, key="txt-record", value="example.com,\"v=spf1 a -all\"")),
        Dnshost(CfgDirective(CfgType.STRING, key="txt-record", value="_http._tcp.example.com,name=value,paper=A4")),
        
        # mx-host
        Dnshost(CfgDirective(CfgType.STRING, key="mx-host", value="maildomain.com")),
        Dnshost(CfgDirective(CfgType.STRING, key="mx-host", value="maildomain.com,servermachine.com")),
        Dnshost(CfgDirective(CfgType.STRING, key="mx-host", value="maildomain.com,servermachine.com,50")),

        # srv-host
        Dnshost(CfgDirective(CfgType.STRING, key="srv-host", value="_ldap._tcp.example.com")),
        Dnshost(CfgDirective(CfgType.STRING, key="srv-host", value="_ldap._tcp.example.com,ldapserver.example.com")),
        Dnshost(CfgDirective(CfgType.STRING, key="srv-host", value="_ldap._tcp.example.com,ldapserver.example.com,389")),
        Dnshost(CfgDirective(CfgType.STRING, key="srv-host", value="_ldap._tcp.example.com,ldapserver.example.com,389,2")),
        Dnshost(CfgDirective(CfgType.STRING, key="srv-host", value="_ldap._tcp.example.com,ldapserver.example.com,389,2,100")),

        # caa
        Dnshost(CfgDirective(CfgType.STRING, key="caa-record", value="example.com,0,issue,digicert.com")),
        
    ]

    #pprint(configs)

    line1 = 'a," b ",c'
    line2 = "a,' b ',c"
    line3 = 'a," b " ,c'

    print(Dnshost.line_split(line1))
    print(Dnshost.line_split(line2))
    print(Dnshost.line_split(line3))
