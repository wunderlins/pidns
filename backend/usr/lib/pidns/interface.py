#!/usr/bin/env python3
from pyroute2 import IPRoute, NDB, IPDB
from pprint import pprint
import platform

system = platform.system()
ipdb = None
if system != "Darwin":
    ipdb = IPDB()

class ipaddr:
    addr = None
    mask = None

    def __init__(self, addr, mask):
        self.addr = addr
        self.mask = mask

class interface:
    index = 0
    iface = ""
    hw_addr = None
    ip_addr4 = []
    ip_addr6 = []
    default_gw = None
    state = None
    linkmode = None

    def __init__(self, index, iface, hw_addr, ip_addr, default_gw, state, linkmode):
        self.index = index
        self.iface = iface
        self.hw_addr = hw_addr
        self.default_gw = default_gw
        self.ip_addr4 = []
        self.ip_addr6 = []
        self.state = state
        self.linkmode = linkmode
        if iface == "lo":
            self.linkmode = -1

        for e in ip_addr:
            n = e[0]
            v = e[1]
            if not n:
                continue
            if ":" in e[0]:
                self.ip_addr6.append(ipaddr(n, v))
            else:
                n = str(e[0])
                self.ip_addr4.append(ipaddr(n, v))

    def display_ip(self):
        if len(self.ip_addr4) == 0:
            return "[]"
        #pprint(self.ip_addr4)
        addr = [x.addr + "/" + str(x.mask) for x in self.ip_addr4]
        #pprint(addr)
        return str(addr)

    def __repr__(self):
        return "<[" + str(self.index) + "] " + \
            self.iface + ": " + str(self.hw_addr) + \
            ", " + self.display_ip() + ", default_gw: " + \
            str(self.default_gw) + ", " + self.state + ">"

def default_gateway(ifname):
    gw = []
    if ipdb == None:
        return gw
    
    for r in ipdb.routes:
        if r["oif"] == None:
            continue
        
        iface = ipdb.interfaces[r["oif"]].ifname
        if iface != ifname:
            continue
        if r["dst"] == "default":
            return r["gateway"]
        #pprint( [r["dst"], r["gateway"], r["oif"], iface])
    
    return None

def get_interfaces():
    ifaces = []

    if ipdb == None:
        return ifaces

    for ifname in ipdb.by_name.keys():
        #print("-> " + ifname)
        i = ipdb.interfaces[ifname]
        """
        pprint(i.index)
        pprint(i.address)
        pprint(i.ipaddr)
        pprint(default_gateway(ifname))
        """

        ifs = interface(i.index, ifname, i.address, i.ipaddr, default_gateway(ifname), i.state, i.linkmode)
        #pprint(i.family)
        #pprint(ifs)
        ifaces.append(ifs)
    return ifaces


if __name__ == "__main__":
    # get access to the netlink socket
    ip = IPRoute()
    # no monitoring here -- thus no bind()

    """
    # print interfaces
    for link in ip.get_links():
        pprint(link)
    """

    """
    for link in ip.get_links():
        #pprint(link["attrs"])
        for k, v in link["attrs"]:
            print("k: " + k + ", v:" + str(v))
    """

    """
    ndb = NDB(log='on')
    for record in ndb.interfaces.summary():
        addr = [
            {
                "iface": x['index'], 
                "addr": x.get_attr('IFA_ADDRESS'),
                "mask": x["prefixlen"]
            } 
            for x in ip.get_addr(index=record.index)
        ]
        mask = []
        ipaddr = []
        try:
            for e in addr:
                mask.append(e["mask"])
                ipaddr.append(e["addr"])
        except:
            pass
        #print(record.index, record.ifname, record.lladdr, ip_addr)
        interfaces.append(interface(
            record.index, 
            record.ifname, 
            record.lladdr, 
            ipaddr,
            mask)
        )
        #pprint(ipaddr)
        #pprint(ip.get_addr(index=record.index))

    pprint(interfaces)
    ipdb = IPDB()
    pprint(ipdb.interfaces)
    """

    """
    ip = IPDB()
    ipr = IPRoute()
    """

    """
    for ifname in ip.by_name.keys():
        print(ifname)
        pprint(ip.interfaces[ifname]["address"])
        pprint(ip.interfaces[ifname]["ipaddr"])
        pprint(ip.interfaces[ifname]["broadcast"])
        pprint(ip.interfaces[ifname]["ifalias"])
        pprint(ip.interfaces[ifname]["index"])
        gw = []
        #try:
        for i in ip.interfaces[ifname]["ipaddr"]:
            print("-> " + str(i[0]))
            #print(ipr.route("get", prefsrc=i[1], dst="8.8.8.8")[0].get_attr("RTA_GATEWAY"))
            gw.append(ipr.route("get", prefsrc=i[0], dst="8.8.8.8"))
        #except: pass
        pprint(gw)
    """

    """
    pprint(["dst", "gateway", "oif", "ifname"])
    for r in ip.routes:
        ifname = ip.interfaces[r["oif"]].ifname
        pprint( [r["dst"], r["gateway"], r["oif"], ifname])
    """

    """
    ifaces = []
    ipdb = IPDB()
    for ifname in ipdb.by_name.keys():
        print("-> " + ifname)
        i = ipdb.interfaces[ifname]
        "" "
        pprint(i.index)
        pprint(i.address)
        pprint(i.ipaddr)
        pprint(default_gateway(ifname))
        " ""

        ifs = interface(i.index, ifname, i.address, i.ipaddr, default_gateway(ifname))
        pprint(ifs)
        ifaces.append(ifs)

    #pprint(ifaces)
    """

    """
    ip = IPDB()
    gw = []
    for r in ip.routes:
        iface = ip.interfaces[r["oif"]].ifname
        pprint([iface, r["dst"], r["gateway"], r["oif"], iface])
    """

    ifaces = get_interfaces()
    pprint(ifaces)

