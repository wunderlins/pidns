from enum import Enum
import sys
from json import JSONEncoder
import copy 


class CfgType(Enum):
    UNDEFINED = 0
    INT = 1
    FLOAT = 2
    STRING = 3
    LIST = 4

class CfgGroups(Enum):
    HIDE = 0
    GENERAL = 1
    NETWOK = 2
    DHCP = 3
    DNS = 4
    OTHER = 5
    HOST = 6

grouping = {
    CfgGroups.HIDE:    ["test", "help", "version"],
    CfgGroups.GENERAL: ["user", "group", "pid-file", "dhcp-leasefile", "conf-file", "conf-dir", "addn-hosts", "no-hosts", "resolv-file", "no-poll", "quiet-dhcp", "quiet-dhcp6", "quiet-ra", "no-resolv", "log-queries", "log-facility"],
    CfgGroups.NETWOK:  ["interface-name", "expand-hosts", "port", "interface", "except-interface", "auth-server", "listen-address", "bind-interfaces", "bind-dynamic", "local", "server", "rev-server"],
    CfgGroups.DHCP:    ["dhcp-fqdn", "dhcp-ttl", "dhcp-range", "no-dhcp-interface", "dhcp-option", "read-ethers", "dhcp-vendorclass", "dhcp-userclass", "dhcp-mac", "dhcp-remoteid", "dhcp-ignore", "dhcp-boot", "dhcp-lease-max", "dhcp-authoritative", "no-ping", "leasefile-ro"],
    CfgGroups.DNS:     ["domain-needed", "domain", "local-service", "max-ttl", "max-cache-ttl", "min-cache-ttl", "auth-ttl", "auth-server", "local-ttl", "dns-rr", "address", "naptr-record", "query-port", "bogus-priv", "alias", "filterwin2k", "strict-order", "mx-target", "selfmx", "localmx", "cache-size", "no-negcache", "dns-forward-max"],
    CfgGroups.OTHER:   [],
    CfgGroups.HOST:    ["dhcp-host", "host-record", "cname", "ptr-record", "srv-host", "txt-record", "mx-host", "caa-record"]
}

groups = [
    {"name": "Hide", "group": CfgGroups.HIDE.value},
    {"name": "General", "group": CfgGroups.GENERAL.value},
    {"name": "Networking", "group": CfgGroups.NETWOK.value},
    {"name": "DCHP", "group": CfgGroups.DHCP.value},
    {"name": "DNS", "group": CfgGroups.DNS.value},
    {"name": "Other", "group": CfgGroups.OTHER.value},
]

class CfgDirective(JSONEncoder):
    type         = CfgType.STRING
    key          = ""
    value        = None
    description  = None
    groups       = []
    exampleValue = None
    options      = [] # special options added as inline comments

    # number of options:
    # - 0 no parameter
    # - 1 only once per config
    # - 2 repeatable
    # - -1 is unknown
    opt          = -1

    def __init__(self, type, key, exampleValue=None, value=None, description=None, groups=[], opt=-1):
        self.type = type
        self.key = key
        self.exampleValue = exampleValue
        self.value = value
        if description != None:
            self.description = description.strip()
        self.groups = groups
        self.options = []
        self.opt    = opt
    
    def __repr__(self):
        return "<" + self.key + "(" + str(self.type) + "): " + \
            str(self.value) + ", " + str(self.groups) + \
            ", options=" + str(self.options) + ">"
    
    def to_code(self):
        sys.stdout.write("CfgDirective.CfgDirective(")
        sys.stdout.write("CfgDirective." + str(self.type))
        sys.stdout.write(", \"")
        sys.stdout.write(str(self.key))
        sys.stdout.write("\", \"")
        sys.stdout.write(self.exampleValue)
        sys.stdout.write("\", \"\", '''")
        sys.stdout.write(self.description)
        sys.stdout.write("''' , ")
        sys.stdout.write("groups=[")
        g = []
        for e in self.groups:
            g.append(str(e.value))
        sys.stdout.write(",".join(g) + "], opt=" + str(self.opt))
        sys.stdout.write("),\n")
    
    def to_json(self):
        d = copy.deepcopy(self).__dict__
        d["type"] = d["type"].value
        return d

