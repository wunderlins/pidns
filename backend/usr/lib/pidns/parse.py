#!/usr/bin/env python3

from enum import Enum
import os, sys
from lib.pidns.CfgDirective import *

# main_file = "../conf/example.conf"

def is_int(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

def is_list(s):
    l = s.split(",")
    return l[0] != s

def parse_config(config_file):
    """parse dnsmasq config files"""


    config = []
    with open(config_file, "r") as a_file:
        buf_comment = ""

        for line in a_file:
            line = line.strip()
            if line == "":
                continue

            if line[0:1] == "#" and line[1:2] == " ":
                buf_comment += line[1:]
                continue

            if line[0:1] != "#":
                # split key=value pairs
                l = line.split("=")
                key = l.pop(0).strip()
                value = None
                try:
                    value = "=".join([x for x in l if x != None and x.strip()])
                except: pass

                # if there is an inline comment, deal with it
                inline_comment = None
                options        = []
                if "#" in value:
                    #print("ya")
                    parts = value.split("#")
                    value = parts.pop(0).strip()
                    inline_comment = "#".join([x for x in parts if x != None and x.strip()]).strip()

                    # is this a pidns option ?
                    if inline_comment[0:9] == "pidns-opt":
                        options = inline_comment[10:].strip().split(",")

                type  = CfgType.STRING
                #print(value, len(value))

                if value != None and is_int(value):
                    value = int(value)
                    type  = CfgType.INT
                #elif value != None and is_list(value):
                #    value = value.split(",")
                #    type  = CfgType.LIST
                
                #print(type, key, value)
                cfg = CfgDirective(type, key, value=value)
                cfg.description = buf_comment
                cfg.options = options
                config.append(cfg)
                #config.append(CfgDirective(type, key, value, buf_comment))
                buf_comment = ""
    return config

if __name__ == "__main__":
    from pprint import pprint
    c = parse_config(sys.argv[1])
    pprint(c)
