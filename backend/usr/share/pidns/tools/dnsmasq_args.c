#include <stdio.h>
#include <stdlib.h>
#include "config.h"

#define ARG_DUP       OPT_LAST
#define ARG_ONE       OPT_LAST + 1
#define ARG_USED_CL   OPT_LAST + 2
#define ARG_USED_FILE OPT_LAST + 3


#define OPT_BOGUSPRIV      0
#define OPT_FILTER         1
#define OPT_LOG            2
#define OPT_SELFMX         3
#define OPT_NO_HOSTS       4
#define OPT_NO_POLL        5
#define OPT_DEBUG          6
#define OPT_ORDER          7
#define OPT_NO_RESOLV      8
#define OPT_EXPAND         9
#define OPT_LOCALMX        10
#define OPT_NO_NEG         11
#define OPT_NODOTS_LOCAL   12
#define OPT_NOWILD         13
#define OPT_ETHERS         14
#define OPT_RESOLV_DOMAIN  15
#define OPT_NO_FORK        16
#define OPT_AUTHORITATIVE  17
#define OPT_LOCALISE       18
#define OPT_DBUS           19
#define OPT_DHCP_FQDN      20
#define OPT_NO_PING        21
#define OPT_LEASE_RO       22
#define OPT_ALL_SERVERS    23
#define OPT_RELOAD         24
#define OPT_LOCAL_REBIND   25  
#define OPT_TFTP_SECURE    26
#define OPT_TFTP_NOBLOCK   27
#define OPT_LOG_OPTS       28
#define OPT_TFTP_APREF_IP  29
#define OPT_NO_OVERRIDE    30
#define OPT_NO_REBIND      31
#define OPT_ADD_MAC        32
#define OPT_DNSSEC_PROXY   33
#define OPT_CONSEC_ADDR    34
#define OPT_CONNTRACK      35
#define OPT_FQDN_UPDATE    36
#define OPT_RA             37
#define OPT_TFTP_LC        38
#define OPT_CLEVERBIND     39
#define OPT_TFTP           40
#define OPT_CLIENT_SUBNET  41
#define OPT_QUIET_DHCP     42
#define OPT_QUIET_DHCP6    43
#define OPT_QUIET_RA	   44
#define OPT_DNSSEC_VALID   45
#define OPT_DNSSEC_TIME    46
#define OPT_DNSSEC_DEBUG   47
#define OPT_DNSSEC_IGN_NS  48 
#define OPT_LOCAL_SERVICE  49
#define OPT_LOOP_DETECT    50
#define OPT_EXTRALOG       51
#define OPT_TFTP_NO_FAIL   52
#define OPT_SCRIPT_ARP     53
#define OPT_MAC_B64        54
#define OPT_MAC_HEX        55
#define OPT_TFTP_APREF_MAC 56
#define OPT_RAPID_COMMIT   57
#define OPT_UBUS           58
#define OPT_IGNORE_CLID    59
#define OPT_SINGLE_PORT    60
#define OPT_LEASE_RENEW    61
#define OPT_LAST           62

/* options which don't have a one-char version */
#define LOPT_RELOAD        256
#define LOPT_NO_NAMES      257
#define LOPT_TFTP          258
#define LOPT_SECURE        259
#define LOPT_PREFIX        260
#define LOPT_PTR           261
#define LOPT_BRIDGE        262
#define LOPT_TFTP_MAX      263
#define LOPT_FORCE         264
#define LOPT_NOBLOCK       265
#define LOPT_LOG_OPTS      266
#define LOPT_MAX_LOGS      267
#define LOPT_CIRCUIT       268
#define LOPT_REMOTE        269
#define LOPT_SUBSCR        270
#define LOPT_INTNAME       271
#define LOPT_BANK          272
#define LOPT_DHCP_HOST     273
#define LOPT_APREF         274
#define LOPT_OVERRIDE      275
#define LOPT_TFTPPORTS     276
#define LOPT_REBIND        277
#define LOPT_NOLAST        278
#define LOPT_OPTS          279
#define LOPT_DHCP_OPTS     280
#define LOPT_MATCH         281
#define LOPT_BROADCAST     282
#define LOPT_NEGTTL        283
#define LOPT_ALTPORT       284
#define LOPT_SCRIPTUSR     285
#define LOPT_LOCAL         286
#define LOPT_NAPTR         287
#define LOPT_MINPORT       288
#define LOPT_DHCP_FQDN     289
#define LOPT_CNAME         290
#define LOPT_PXE_PROMT     291
#define LOPT_PXE_SERV      292
#define LOPT_TEST          293
#define LOPT_TAG_IF        294
#define LOPT_PROXY         295
#define LOPT_GEN_NAMES     296
#define LOPT_MAXTTL        297
#define LOPT_NO_REBIND     298
#define LOPT_LOC_REBND     299
#define LOPT_ADD_MAC       300
#define LOPT_DNSSEC        301
#define LOPT_INCR_ADDR     302
#define LOPT_CONNTRACK     303
#define LOPT_FQDN          304
#define LOPT_LUASCRIPT     305
#define LOPT_RA            306
#define LOPT_DUID          307
#define LOPT_HOST_REC      308
#define LOPT_TFTP_LC       309
#define LOPT_RR            310
#define LOPT_CLVERBIND     311
#define LOPT_MAXCTTL       312
#define LOPT_AUTHZONE      313
#define LOPT_AUTHSERV      314
#define LOPT_AUTHTTL       315
#define LOPT_AUTHSOA       316
#define LOPT_AUTHSFS       317
#define LOPT_AUTHPEER      318
#define LOPT_IPSET         319
#define LOPT_SYNTH         320
#define LOPT_RELAY         323
#define LOPT_RA_PARAM      324
#define LOPT_ADD_SBNET     325
#define LOPT_QUIET_DHCP    326
#define LOPT_QUIET_DHCP6   327
#define LOPT_QUIET_RA      328
#define LOPT_SEC_VALID     329
#define LOPT_TRUST_ANCHOR  330
#define LOPT_DNSSEC_DEBUG  331
#define LOPT_REV_SERV      332
#define LOPT_SERVERS_FILE  333
#define LOPT_DNSSEC_CHECK  334
#define LOPT_LOCAL_SERVICE 335
#define LOPT_DNSSEC_TIME   336
#define LOPT_LOOP_DETECT   337
#define LOPT_IGNORE_ADDR   338
#define LOPT_MINCTTL       339
#define LOPT_DHCP_INOTIFY  340
#define LOPT_DHOPT_INOTIFY 341
#define LOPT_HOST_INOTIFY  342
#define LOPT_DNSSEC_STAMP  343
#define LOPT_TFTP_NO_FAIL  344
#define LOPT_MAXPORT       345
#define LOPT_CPE_ID        346
#define LOPT_SCRIPT_ARP    347
#define LOPT_DHCPTTL       348
#define LOPT_TFTP_MTU      349
#define LOPT_REPLY_DELAY   350
#define LOPT_RAPID_COMMIT  351
#define LOPT_DUMPFILE      352
#define LOPT_DUMPMASK      353
#define LOPT_UBUS          354
#define LOPT_NAME_MATCH    355
#define LOPT_CAA           356
#define LOPT_SHARED_NET    357
#define LOPT_IGNORE_CLID   358
#define LOPT_SINGLE_PORT   359
#define LOPT_SCRIPT_TIME   360

struct myoption {
  const char *name;
  int has_arg;
  int *flag;
  int val;
};

static const struct myoption opts[] = 
  { 
    { "version", 0, 0, 'v' },
    { "no-hosts", 0, 0, 'h' },
    { "no-poll", 0, 0, 'n' },
    { "help", 0, 0, 'w' },
    { "no-daemon", 0, 0, 'd' },
    { "log-queries", 2, 0, 'q' },
    { "user", 2, 0, 'u' },
    { "group", 2, 0, 'g' },
    { "resolv-file", 2, 0, 'r' },
    { "servers-file", 1, 0, LOPT_SERVERS_FILE },
    { "mx-host", 1, 0, 'm' },
    { "mx-target", 1, 0, 't' },
    { "cache-size", 2, 0, 'c' },
    { "port", 1, 0, 'p' },
    { "dhcp-leasefile", 2, 0, 'l' },
    { "dhcp-lease", 1, 0, 'l' },
    { "dhcp-host", 1, 0, 'G' },
    { "dhcp-range", 1, 0, 'F' },
    { "dhcp-option", 1, 0, 'O' },
    { "dhcp-boot", 1, 0, 'M' },
    { "domain", 1, 0, 's' },
    { "domain-suffix", 1, 0, 's' },
    { "interface", 1, 0, 'i' },
    { "listen-address", 1, 0, 'a' },
    { "local-service", 0, 0, LOPT_LOCAL_SERVICE },
    { "bogus-priv", 0, 0, 'b' },
    { "bogus-nxdomain", 1, 0, 'B' },
    { "ignore-address", 1, 0, LOPT_IGNORE_ADDR },
    { "selfmx", 0, 0, 'e' },
    { "filterwin2k", 0, 0, 'f' },
    { "pid-file", 2, 0, 'x' },
    { "strict-order", 0, 0, 'o' },
    { "server", 1, 0, 'S' },
    { "rev-server", 1, 0, LOPT_REV_SERV },
    { "local", 1, 0, LOPT_LOCAL },
    { "address", 1, 0, 'A' },
    { "conf-file", 2, 0, 'C' },
    { "no-resolv", 0, 0, 'R' },
    { "expand-hosts", 0, 0, 'E' },
    { "localmx", 0, 0, 'L' },
    { "local-ttl", 1, 0, 'T' },
    { "no-negcache", 0, 0, 'N' },
    { "addn-hosts", 1, 0, 'H' },
    { "hostsdir", 1, 0, LOPT_HOST_INOTIFY },
    { "query-port", 1, 0, 'Q' },
    { "except-interface", 1, 0, 'I' },
    { "no-dhcp-interface", 1, 0, '2' },
    { "domain-needed", 0, 0, 'D' },
    { "dhcp-lease-max", 1, 0, 'X' },
    { "bind-interfaces", 0, 0, 'z' },
    { "read-ethers", 0, 0, 'Z' },
    { "alias", 1, 0, 'V' },
    { "dhcp-vendorclass", 1, 0, 'U' },
    { "dhcp-userclass", 1, 0, 'j' },
    { "dhcp-ignore", 1, 0, 'J' },
    { "edns-packet-max", 1, 0, 'P' },
    { "keep-in-foreground", 0, 0, 'k' },
    { "dhcp-authoritative", 0, 0, 'K' },
    { "srv-host", 1, 0, 'W' },
    { "localise-queries", 0, 0, 'y' },
    { "txt-record", 1, 0, 'Y' },
    { "caa-record", 1, 0 , LOPT_CAA },
    { "dns-rr", 1, 0, LOPT_RR },
    { "enable-dbus", 2, 0, '1' },
    { "enable-ubus", 2, 0, LOPT_UBUS },
    { "bootp-dynamic", 2, 0, '3' },
    { "dhcp-mac", 1, 0, '4' },
    { "no-ping", 0, 0, '5' },
    { "dhcp-script", 1, 0, '6' },
    { "conf-dir", 1, 0, '7' },
    { "log-facility", 1, 0 ,'8' },
    { "leasefile-ro", 0, 0, '9' },
    { "script-on-renewal", 0, 0, LOPT_SCRIPT_TIME},
    { "dns-forward-max", 1, 0, '0' },
    { "clear-on-reload", 0, 0, LOPT_RELOAD },
    { "dhcp-ignore-names", 2, 0, LOPT_NO_NAMES },
    { "enable-tftp", 2, 0, LOPT_TFTP },
    { "tftp-secure", 0, 0, LOPT_SECURE },
    { "tftp-no-fail", 0, 0, LOPT_TFTP_NO_FAIL },
    { "tftp-unique-root", 2, 0, LOPT_APREF },
    { "tftp-root", 1, 0, LOPT_PREFIX },
    { "tftp-max", 1, 0, LOPT_TFTP_MAX },
    { "tftp-mtu", 1, 0, LOPT_TFTP_MTU },
    { "tftp-lowercase", 0, 0, LOPT_TFTP_LC },
    { "tftp-single-port", 0, 0, LOPT_SINGLE_PORT },
    { "ptr-record", 1, 0, LOPT_PTR },
    { "naptr-record", 1, 0, LOPT_NAPTR },
    { "bridge-interface", 1, 0 , LOPT_BRIDGE },
    { "shared-network", 1, 0, LOPT_SHARED_NET },
    { "dhcp-option-force", 1, 0, LOPT_FORCE },
    { "tftp-no-blocksize", 0, 0, LOPT_NOBLOCK },
    { "log-dhcp", 0, 0, LOPT_LOG_OPTS },
    { "log-async", 2, 0, LOPT_MAX_LOGS },
    { "dhcp-circuitid", 1, 0, LOPT_CIRCUIT },
    { "dhcp-remoteid", 1, 0, LOPT_REMOTE },
    { "dhcp-subscrid", 1, 0, LOPT_SUBSCR },
    { "interface-name", 1, 0, LOPT_INTNAME },
    { "dhcp-hostsfile", 1, 0, LOPT_DHCP_HOST },
    { "dhcp-optsfile", 1, 0, LOPT_DHCP_OPTS },
    { "dhcp-hostsdir", 1, 0, LOPT_DHCP_INOTIFY },
    { "dhcp-optsdir", 1, 0, LOPT_DHOPT_INOTIFY },
    { "dhcp-no-override", 0, 0, LOPT_OVERRIDE },
    { "tftp-port-range", 1, 0, LOPT_TFTPPORTS },
    { "stop-dns-rebind", 0, 0, LOPT_REBIND },
    { "rebind-domain-ok", 1, 0, LOPT_NO_REBIND },
    { "all-servers", 0, 0, LOPT_NOLAST }, 
    { "dhcp-match", 1, 0, LOPT_MATCH },
    { "dhcp-name-match", 1, 0, LOPT_NAME_MATCH },
    { "dhcp-broadcast", 2, 0, LOPT_BROADCAST },
    { "neg-ttl", 1, 0, LOPT_NEGTTL },
    { "max-ttl", 1, 0, LOPT_MAXTTL },
    { "min-cache-ttl", 1, 0, LOPT_MINCTTL },
    { "max-cache-ttl", 1, 0, LOPT_MAXCTTL },
    { "dhcp-alternate-port", 2, 0, LOPT_ALTPORT },
    { "dhcp-scriptuser", 1, 0, LOPT_SCRIPTUSR },
    { "min-port", 1, 0, LOPT_MINPORT },
    { "max-port", 1, 0, LOPT_MAXPORT },
    { "dhcp-fqdn", 0, 0, LOPT_DHCP_FQDN },
    { "cname", 1, 0, LOPT_CNAME },
    { "pxe-prompt", 1, 0, LOPT_PXE_PROMT },
    { "pxe-service", 1, 0, LOPT_PXE_SERV },
    { "test", 0, 0, LOPT_TEST },
    { "tag-if", 1, 0, LOPT_TAG_IF },
    { "dhcp-proxy", 2, 0, LOPT_PROXY },
    { "dhcp-generate-names", 2, 0, LOPT_GEN_NAMES },
    { "rebind-localhost-ok", 0, 0,  LOPT_LOC_REBND },
    { "add-mac", 2, 0, LOPT_ADD_MAC },
    { "add-subnet", 2, 0, LOPT_ADD_SBNET },
    { "add-cpe-id", 1, 0 , LOPT_CPE_ID },
    { "proxy-dnssec", 0, 0, LOPT_DNSSEC },
    { "dhcp-sequential-ip", 0, 0,  LOPT_INCR_ADDR },
    { "conntrack", 0, 0, LOPT_CONNTRACK },
    { "dhcp-client-update", 0, 0, LOPT_FQDN },
    { "dhcp-luascript", 1, 0, LOPT_LUASCRIPT },
    { "enable-ra", 0, 0, LOPT_RA },
    { "dhcp-duid", 1, 0, LOPT_DUID },
    { "host-record", 1, 0, LOPT_HOST_REC },
    { "bind-dynamic", 0, 0, LOPT_CLVERBIND },
    { "auth-zone", 1, 0, LOPT_AUTHZONE },
    { "auth-server", 1, 0, LOPT_AUTHSERV },
    { "auth-ttl", 1, 0, LOPT_AUTHTTL },
    { "auth-soa", 1, 0, LOPT_AUTHSOA },
    { "auth-sec-servers", 1, 0, LOPT_AUTHSFS },
    { "auth-peer", 1, 0, LOPT_AUTHPEER }, 
    { "ipset", 1, 0, LOPT_IPSET },
    { "synth-domain", 1, 0, LOPT_SYNTH },
    { "dnssec", 0, 0, LOPT_SEC_VALID },
    { "trust-anchor", 1, 0, LOPT_TRUST_ANCHOR },
    { "dnssec-debug", 0, 0, LOPT_DNSSEC_DEBUG },
    { "dnssec-check-unsigned", 2, 0, LOPT_DNSSEC_CHECK },
    { "dnssec-no-timecheck", 0, 0, LOPT_DNSSEC_TIME },
    { "dnssec-timestamp", 1, 0, LOPT_DNSSEC_STAMP },
    { "dhcp-relay", 1, 0, LOPT_RELAY },
    { "ra-param", 1, 0, LOPT_RA_PARAM },
    { "quiet-dhcp", 0, 0, LOPT_QUIET_DHCP },
    { "quiet-dhcp6", 0, 0, LOPT_QUIET_DHCP6 },
    { "quiet-ra", 0, 0, LOPT_QUIET_RA },
    { "dns-loop-detect", 0, 0, LOPT_LOOP_DETECT },
    { "script-arp", 0, 0, LOPT_SCRIPT_ARP },
    { "dhcp-ttl", 1, 0 , LOPT_DHCPTTL },
    { "dhcp-reply-delay", 1, 0, LOPT_REPLY_DELAY },
    { "dhcp-rapid-commit", 0, 0, LOPT_RAPID_COMMIT },
    { "dumpfile", 1, 0, LOPT_DUMPFILE },
    { "dumpmask", 1, 0, LOPT_DUMPMASK },
    { "dhcp-ignore-clid", 0, 0,  LOPT_IGNORE_CLID },
    { NULL, 0, 0, 0 }
  };

/*
char *gettext_noop(char *str) {
    return NULL;
}
*/

#define gettext_noop(S) (S)

static struct {
  int opt;
  unsigned int rept;
  char * const flagdesc;
  char * const desc;
  char * const arg;
} usage[] = {
  { 'a', ARG_DUP, "<ipaddr>",  gettext_noop("Specify local address(es) to listen on."), NULL },
  { 'A', ARG_DUP, "/<domain>/<ipaddr>", gettext_noop("Return ipaddr for all hosts in specified domains."), NULL },
  { 'b', OPT_BOGUSPRIV, NULL, gettext_noop("Fake reverse lookups for RFC1918 private address ranges."), NULL },
  { 'B', ARG_DUP, "<ipaddr>", gettext_noop("Treat ipaddr as NXDOMAIN (defeats Verisign wildcard)."), NULL }, 
  { 'c', ARG_ONE, "<integer>", gettext_noop("Specify the size of the cache in entries (defaults to %s)."), "$" },
  { 'C', ARG_DUP, "<path>", gettext_noop("Specify configuration file (defaults to %s)."), CONFFILE },
  { 'd', OPT_DEBUG, NULL, gettext_noop("Do NOT fork into the background: run in debug mode."), NULL },
  { 'D', OPT_NODOTS_LOCAL, NULL, gettext_noop("Do NOT forward queries with no domain part."), NULL }, 
  { 'e', OPT_SELFMX, NULL, gettext_noop("Return self-pointing MX records for local hosts."), NULL },
  { 'E', OPT_EXPAND, NULL, gettext_noop("Expand simple names in /etc/hosts with domain-suffix."), NULL },
  { 'f', OPT_FILTER, NULL, gettext_noop("Don't forward spurious DNS requests from Windows hosts."), NULL },
  { 'F', ARG_DUP, "<ipaddr>,...", gettext_noop("Enable DHCP in the range given with lease duration."), NULL },
  { 'g', ARG_ONE, "<groupname>", gettext_noop("Change to this group after startup (defaults to %s)."), CHGRP },
  { 'G', ARG_DUP, "<hostspec>", gettext_noop("Set address or hostname for a specified machine."), NULL },
  { LOPT_DHCP_HOST, ARG_DUP, "<path>", gettext_noop("Read DHCP host specs from file."), NULL },
  { LOPT_DHCP_OPTS, ARG_DUP, "<path>", gettext_noop("Read DHCP option specs from file."), NULL },
  { LOPT_DHCP_INOTIFY, ARG_DUP, "<path>", gettext_noop("Read DHCP host specs from a directory."), NULL }, 
  { LOPT_DHOPT_INOTIFY, ARG_DUP, "<path>", gettext_noop("Read DHCP options from a directory."), NULL }, 
  { LOPT_TAG_IF, ARG_DUP, "tag-expression", gettext_noop("Evaluate conditional tag expression."), NULL },
  { 'h', OPT_NO_HOSTS, NULL, gettext_noop("Do NOT load %s file."), HOSTSFILE },
  { 'H', ARG_DUP, "<path>", gettext_noop("Specify a hosts file to be read in addition to %s."), HOSTSFILE },
  { LOPT_HOST_INOTIFY, ARG_DUP, "<path>", gettext_noop("Read hosts files from a directory."), NULL },
  { 'i', ARG_DUP, "<interface>", gettext_noop("Specify interface(s) to listen on."), NULL },
  { 'I', ARG_DUP, "<interface>", gettext_noop("Specify interface(s) NOT to listen on.") , NULL },
  { 'j', ARG_DUP, "set:<tag>,<class>", gettext_noop("Map DHCP user class to tag."), NULL },
  { LOPT_CIRCUIT, ARG_DUP, "set:<tag>,<circuit>", gettext_noop("Map RFC3046 circuit-id to tag."), NULL },
  { LOPT_REMOTE, ARG_DUP, "set:<tag>,<remote>", gettext_noop("Map RFC3046 remote-id to tag."), NULL },
  { LOPT_SUBSCR, ARG_DUP, "set:<tag>,<remote>", gettext_noop("Map RFC3993 subscriber-id to tag."), NULL },
  { 'J', ARG_DUP, "tag:<tag>...", gettext_noop("Don't do DHCP for hosts with tag set."), NULL },
  { LOPT_BROADCAST, ARG_DUP, "[=tag:<tag>...]", gettext_noop("Force broadcast replies for hosts with tag set."), NULL }, 
  { 'k', OPT_NO_FORK, NULL, gettext_noop("Do NOT fork into the background, do NOT run in debug mode."), NULL },
  { 'K', OPT_AUTHORITATIVE, NULL, gettext_noop("Assume we are the only DHCP server on the local network."), NULL },
  { 'l', ARG_ONE, "<path>", gettext_noop("Specify where to store DHCP leases (defaults to %s)."), LEASEFILE },
  { 'L', OPT_LOCALMX, NULL, gettext_noop("Return MX records for local hosts."), NULL },
  { 'm', ARG_DUP, "<host_name>,<target>,<pref>", gettext_noop("Specify an MX record."), NULL },
  { 'M', ARG_DUP, "<bootp opts>", gettext_noop("Specify BOOTP options to DHCP server."), NULL },
  { 'n', OPT_NO_POLL, NULL, gettext_noop("Do NOT poll %s file, reload only on SIGHUP."), RESOLVFILE }, 
  { 'N', OPT_NO_NEG, NULL, gettext_noop("Do NOT cache failed search results."), NULL },
  { 'o', OPT_ORDER, NULL, gettext_noop("Use nameservers strictly in the order given in %s."), RESOLVFILE },
  { 'O', ARG_DUP, "<optspec>", gettext_noop("Specify options to be sent to DHCP clients."), NULL },
  { LOPT_FORCE, ARG_DUP, "<optspec>", gettext_noop("DHCP option sent even if the client does not request it."), NULL},
  { 'p', ARG_ONE, "<integer>", gettext_noop("Specify port to listen for DNS requests on (defaults to 53)."), NULL },
  { 'P', ARG_ONE, "<integer>", gettext_noop("Maximum supported UDP packet size for EDNS.0 (defaults to %s)."), "*" },
  { 'q', ARG_DUP, NULL, gettext_noop("Log DNS queries."), NULL },
  { 'Q', ARG_ONE, "<integer>", gettext_noop("Force the originating port for upstream DNS queries."), NULL },
  { 'R', OPT_NO_RESOLV, NULL, gettext_noop("Do NOT read resolv.conf."), NULL },
  { 'r', ARG_DUP, "<path>", gettext_noop("Specify path to resolv.conf (defaults to %s)."), RESOLVFILE }, 
  { LOPT_SERVERS_FILE, ARG_ONE, "<path>", gettext_noop("Specify path to file with server= options"), NULL },
  { 'S', ARG_DUP, "/<domain>/<ipaddr>", gettext_noop("Specify address(es) of upstream servers with optional domains."), NULL },
  { LOPT_REV_SERV, ARG_DUP, "<addr>/<prefix>,<ipaddr>", gettext_noop("Specify address of upstream servers for reverse address queries"), NULL },
  { LOPT_LOCAL, ARG_DUP, "/<domain>/", gettext_noop("Never forward queries to specified domains."), NULL },
  { 's', ARG_DUP, "<domain>[,<range>]", gettext_noop("Specify the domain to be assigned in DHCP leases."), NULL },
  { 't', ARG_ONE, "<host_name>", gettext_noop("Specify default target in an MX record."), NULL },
  { 'T', ARG_ONE, "<integer>", gettext_noop("Specify time-to-live in seconds for replies from /etc/hosts."), NULL },
  { LOPT_NEGTTL, ARG_ONE, "<integer>", gettext_noop("Specify time-to-live in seconds for negative caching."), NULL },
  { LOPT_MAXTTL, ARG_ONE, "<integer>", gettext_noop("Specify time-to-live in seconds for maximum TTL to send to clients."), NULL },
  { LOPT_MAXCTTL, ARG_ONE, "<integer>", gettext_noop("Specify time-to-live ceiling for cache."), NULL },
  { LOPT_MINCTTL, ARG_ONE, "<integer>", gettext_noop("Specify time-to-live floor for cache."), NULL },
  { 'u', ARG_ONE, "<username>", gettext_noop("Change to this user after startup. (defaults to %s)."), CHUSER }, 
  { 'U', ARG_DUP, "set:<tag>,<class>", gettext_noop("Map DHCP vendor class to tag."), NULL },
  { 'v', 0, NULL, gettext_noop("Display dnsmasq version and copyright information."), NULL },
  { 'V', ARG_DUP, "<ipaddr>,<ipaddr>,<netmask>", gettext_noop("Translate IPv4 addresses from upstream servers."), NULL },
  { 'W', ARG_DUP, "<name>,<target>,...", gettext_noop("Specify a SRV record."), NULL },
  { 'w', 0, NULL, gettext_noop("Display this message. Use --help dhcp or --help dhcp6 for known DHCP options."), NULL },
  { 'x', ARG_ONE, "<path>", gettext_noop("Specify path of PID file (defaults to %s)."), RUNFILE },
  { 'X', ARG_ONE, "<integer>", gettext_noop("Specify maximum number of DHCP leases (defaults to %s)."), "&" },
  { 'y', OPT_LOCALISE, NULL, gettext_noop("Answer DNS queries based on the interface a query was sent to."), NULL },
  { 'Y', ARG_DUP, "<name>,<txt>[,<txt]", gettext_noop("Specify TXT DNS record."), NULL },
  { LOPT_PTR, ARG_DUP, "<name>,<target>", gettext_noop("Specify PTR DNS record."), NULL },
  { LOPT_INTNAME, ARG_DUP, "<name>,<interface>", gettext_noop("Give DNS name to IPv4 address of interface."), NULL },
  { 'z', OPT_NOWILD, NULL, gettext_noop("Bind only to interfaces in use."), NULL },
  { 'Z', OPT_ETHERS, NULL, gettext_noop("Read DHCP static host information from %s."), ETHERSFILE },
  { '1', ARG_ONE, "[=<busname>]", gettext_noop("Enable the DBus interface for setting upstream servers, etc."), NULL },
  { LOPT_UBUS, ARG_ONE, "[=<busname>]", gettext_noop("Enable the UBus interface."), NULL },
  { '2', ARG_DUP, "<interface>", gettext_noop("Do not provide DHCP on this interface, only provide DNS."), NULL },
  { '3', ARG_DUP, "[=tag:<tag>]...", gettext_noop("Enable dynamic address allocation for bootp."), NULL },
  { '4', ARG_DUP, "set:<tag>,<mac address>", gettext_noop("Map MAC address (with wildcards) to option set."), NULL },
  { LOPT_BRIDGE, ARG_DUP, "<iface>,<alias>..", gettext_noop("Treat DHCP requests on aliases as arriving from interface."), NULL },
  { LOPT_SHARED_NET, ARG_DUP, "<iface>|<addr>,<addr>", gettext_noop("Specify extra networks sharing a broadcast domain for DHCP"), NULL},
  { '5', OPT_NO_PING, NULL, gettext_noop("Disable ICMP echo address checking in the DHCP server."), NULL },
  { '6', ARG_ONE, "<path>", gettext_noop("Shell script to run on DHCP lease creation and destruction."), NULL },
  { LOPT_LUASCRIPT, ARG_DUP, "path", gettext_noop("Lua script to run on DHCP lease creation and destruction."), NULL },
  { LOPT_SCRIPTUSR, ARG_ONE, "<username>", gettext_noop("Run lease-change scripts as this user."), NULL },
  { LOPT_SCRIPT_ARP, OPT_SCRIPT_ARP, NULL, gettext_noop("Call dhcp-script with changes to local ARP table."), NULL },
  { '7', ARG_DUP, "<path>", gettext_noop("Read configuration from all the files in this directory."), NULL },
  { '8', ARG_ONE, "<facility>|<file>", gettext_noop("Log to this syslog facility or file. (defaults to DAEMON)"), NULL },
  { '9', OPT_LEASE_RO, NULL, gettext_noop("Do not use leasefile."), NULL },
  { '0', ARG_ONE, "<integer>", gettext_noop("Maximum number of concurrent DNS queries. (defaults to %s)"), "!" }, 
  { LOPT_RELOAD, OPT_RELOAD, NULL, gettext_noop("Clear DNS cache when reloading %s."), RESOLVFILE },
  { LOPT_NO_NAMES, ARG_DUP, "[=tag:<tag>]...", gettext_noop("Ignore hostnames provided by DHCP clients."), NULL },
  { LOPT_OVERRIDE, OPT_NO_OVERRIDE, NULL, gettext_noop("Do NOT reuse filename and server fields for extra DHCP options."), NULL },
  { LOPT_TFTP, ARG_DUP, "[=<intr>[,<intr>]]", gettext_noop("Enable integrated read-only TFTP server."), NULL },
  { LOPT_PREFIX, ARG_DUP, "<dir>[,<iface>]", gettext_noop("Export files by TFTP only from the specified subtree."), NULL },
  { LOPT_APREF, ARG_DUP, "[=ip|mac]", gettext_noop("Add client IP or hardware address to tftp-root."), NULL },
  { LOPT_SECURE, OPT_TFTP_SECURE, NULL, gettext_noop("Allow access only to files owned by the user running dnsmasq."), NULL },
  { LOPT_TFTP_NO_FAIL, OPT_TFTP_NO_FAIL, NULL, gettext_noop("Do not terminate the service if TFTP directories are inaccessible."), NULL },
  { LOPT_TFTP_MAX, ARG_ONE, "<integer>", gettext_noop("Maximum number of concurrent TFTP transfers (defaults to %s)."), "#" },
  { LOPT_TFTP_MTU, ARG_ONE, "<integer>", gettext_noop("Maximum MTU to use for TFTP transfers."), NULL },
  { LOPT_NOBLOCK, OPT_TFTP_NOBLOCK, NULL, gettext_noop("Disable the TFTP blocksize extension."), NULL },
  { LOPT_TFTP_LC, OPT_TFTP_LC, NULL, gettext_noop("Convert TFTP filenames to lowercase"), NULL },
  { LOPT_TFTPPORTS, ARG_ONE, "<start>,<end>", gettext_noop("Ephemeral port range for use by TFTP transfers."), NULL },
  { LOPT_SINGLE_PORT, OPT_SINGLE_PORT, NULL, gettext_noop("Use only one port for TFTP server."), NULL },
  { LOPT_LOG_OPTS, OPT_LOG_OPTS, NULL, gettext_noop("Extra logging for DHCP."), NULL },
  { LOPT_MAX_LOGS, ARG_ONE, "[=<integer>]", gettext_noop("Enable async. logging; optionally set queue length."), NULL },
  { LOPT_REBIND, OPT_NO_REBIND, NULL, gettext_noop("Stop DNS rebinding. Filter private IP ranges when resolving."), NULL },
  { LOPT_LOC_REBND, OPT_LOCAL_REBIND, NULL, gettext_noop("Allow rebinding of 127.0.0.0/8, for RBL servers."), NULL },
  { LOPT_NO_REBIND, ARG_DUP, "/<domain>/", gettext_noop("Inhibit DNS-rebind protection on this domain."), NULL },
  { LOPT_NOLAST, OPT_ALL_SERVERS, NULL, gettext_noop("Always perform DNS queries to all servers."), NULL },
  { LOPT_MATCH, ARG_DUP, "set:<tag>,<optspec>", gettext_noop("Set tag if client includes matching option in request."), NULL },
  { LOPT_NAME_MATCH, ARG_DUP, "set:<tag>,<string>[*]", gettext_noop("Set tag if client provides given name."), NULL },
  { LOPT_ALTPORT, ARG_ONE, "[=<ports>]", gettext_noop("Use alternative ports for DHCP."), NULL },
  { LOPT_NAPTR, ARG_DUP, "<name>,<naptr>", gettext_noop("Specify NAPTR DNS record."), NULL },
  { LOPT_MINPORT, ARG_ONE, "<port>", gettext_noop("Specify lowest port available for DNS query transmission."), NULL },
  { LOPT_MAXPORT, ARG_ONE, "<port>", gettext_noop("Specify highest port available for DNS query transmission."), NULL },
  { LOPT_DHCP_FQDN, OPT_DHCP_FQDN, NULL, gettext_noop("Use only fully qualified domain names for DHCP clients."), NULL },
  { LOPT_GEN_NAMES, ARG_DUP, "[=tag:<tag>]", gettext_noop("Generate hostnames based on MAC address for nameless clients."), NULL},
  { LOPT_PROXY, ARG_DUP, "[=<ipaddr>]...", gettext_noop("Use these DHCP relays as full proxies."), NULL },
  { LOPT_RELAY, ARG_DUP, "<local-addr>,<server>[,<iface>]", gettext_noop("Relay DHCP requests to a remote server"), NULL},
  { LOPT_CNAME, ARG_DUP, "<alias>,<target>[,<ttl>]", gettext_noop("Specify alias name for LOCAL DNS name."), NULL },
  { LOPT_PXE_PROMT, ARG_DUP, "<prompt>,[<timeout>]", gettext_noop("Prompt to send to PXE clients."), NULL },
  { LOPT_PXE_SERV, ARG_DUP, "<service>", gettext_noop("Boot service for PXE menu."), NULL },
  { LOPT_TEST, 0, NULL, gettext_noop("Check configuration syntax."), NULL },
  { LOPT_ADD_MAC, ARG_DUP, "[=base64|text]", gettext_noop("Add requestor's MAC address to forwarded DNS queries."), NULL },
  { LOPT_ADD_SBNET, ARG_ONE, "<v4 pref>[,<v6 pref>]", gettext_noop("Add specified IP subnet to forwarded DNS queries."), NULL },
  { LOPT_CPE_ID, ARG_ONE, "<text>", gettext_noop("Add client identification to forwarded DNS queries."), NULL },
  { LOPT_DNSSEC, OPT_DNSSEC_PROXY, NULL, gettext_noop("Proxy DNSSEC validation results from upstream nameservers."), NULL },
  { LOPT_INCR_ADDR, OPT_CONSEC_ADDR, NULL, gettext_noop("Attempt to allocate sequential IP addresses to DHCP clients."), NULL },
  { LOPT_IGNORE_CLID, OPT_IGNORE_CLID, NULL, gettext_noop("Ignore client identifier option sent by DHCP clients."), NULL },
  { LOPT_CONNTRACK, OPT_CONNTRACK, NULL, gettext_noop("Copy connection-track mark from queries to upstream connections."), NULL },
  { LOPT_FQDN, OPT_FQDN_UPDATE, NULL, gettext_noop("Allow DHCP clients to do their own DDNS updates."), NULL },
  { LOPT_RA, OPT_RA, NULL, gettext_noop("Send router-advertisements for interfaces doing DHCPv6"), NULL },
  { LOPT_DUID, ARG_ONE, "<enterprise>,<duid>", gettext_noop("Specify DUID_EN-type DHCPv6 server DUID"), NULL },
  { LOPT_HOST_REC, ARG_DUP, "<name>,<address>[,<ttl>]", gettext_noop("Specify host (A/AAAA and PTR) records"), NULL },
  { LOPT_CAA, ARG_DUP, "<name>,<flags>,<tag>,<value>", gettext_noop("Specify certification authority authorization record"), NULL },  
  { LOPT_RR, ARG_DUP, "<name>,<RR-number>,[<data>]", gettext_noop("Specify arbitrary DNS resource record"), NULL },
  { LOPT_CLVERBIND, OPT_CLEVERBIND, NULL, gettext_noop("Bind to interfaces in use - check for new interfaces"), NULL },
  { LOPT_AUTHSERV, ARG_ONE, "<NS>,<interface>", gettext_noop("Export local names to global DNS"), NULL },
  { LOPT_AUTHZONE, ARG_DUP, "<domain>,[<subnet>...]", gettext_noop("Domain to export to global DNS"), NULL },
  { LOPT_AUTHTTL, ARG_ONE, "<integer>", gettext_noop("Set TTL for authoritative replies"), NULL },
  { LOPT_AUTHSOA, ARG_ONE, "<serial>[,...]", gettext_noop("Set authoritative zone information"), NULL },
  { LOPT_AUTHSFS, ARG_DUP, "<NS>[,<NS>...]", gettext_noop("Secondary authoritative nameservers for forward domains"), NULL },
  { LOPT_AUTHPEER, ARG_DUP, "<ipaddr>[,<ipaddr>...]", gettext_noop("Peers which are allowed to do zone transfer"), NULL },
  { LOPT_IPSET, ARG_DUP, "/<domain>[/<domain>...]/<ipset>...", gettext_noop("Specify ipsets to which matching domains should be added"), NULL },
  { LOPT_SYNTH, ARG_DUP, "<domain>,<range>,[<prefix>]", gettext_noop("Specify a domain and address range for synthesised names"), NULL },
  { LOPT_SEC_VALID, OPT_DNSSEC_VALID, NULL, gettext_noop("Activate DNSSEC validation"), NULL },
  { LOPT_TRUST_ANCHOR, ARG_DUP, "<domain>,[<class>],...", gettext_noop("Specify trust anchor key digest."), NULL },
  { LOPT_DNSSEC_DEBUG, OPT_DNSSEC_DEBUG, NULL, gettext_noop("Disable upstream checking for DNSSEC debugging."), NULL },
  { LOPT_DNSSEC_CHECK, ARG_DUP, NULL, gettext_noop("Ensure answers without DNSSEC are in unsigned zones."), NULL },
  { LOPT_DNSSEC_TIME, OPT_DNSSEC_TIME, NULL, gettext_noop("Don't check DNSSEC signature timestamps until first cache-reload"), NULL },
  { LOPT_DNSSEC_STAMP, ARG_ONE, "<path>", gettext_noop("Timestamp file to verify system clock for DNSSEC"), NULL },
  { LOPT_RA_PARAM, ARG_DUP, "<iface>,[mtu:<value>|<interface>|off,][<prio>,]<intval>[,<lifetime>]", gettext_noop("Set MTU, priority, resend-interval and router-lifetime"), NULL },
  { LOPT_QUIET_DHCP, OPT_QUIET_DHCP, NULL, gettext_noop("Do not log routine DHCP."), NULL },
  { LOPT_QUIET_DHCP6, OPT_QUIET_DHCP6, NULL, gettext_noop("Do not log routine DHCPv6."), NULL },
  { LOPT_QUIET_RA, OPT_QUIET_RA, NULL, gettext_noop("Do not log RA."), NULL },
  { LOPT_LOCAL_SERVICE, OPT_LOCAL_SERVICE, NULL, gettext_noop("Accept queries only from directly-connected networks."), NULL },
  { LOPT_LOOP_DETECT, OPT_LOOP_DETECT, NULL, gettext_noop("Detect and remove DNS forwarding loops."), NULL },
  { LOPT_IGNORE_ADDR, ARG_DUP, "<ipaddr>", gettext_noop("Ignore DNS responses containing ipaddr."), NULL }, 
  { LOPT_DHCPTTL, ARG_ONE, "<ttl>", gettext_noop("Set TTL in DNS responses with DHCP-derived addresses."), NULL }, 
  { LOPT_REPLY_DELAY, ARG_ONE, "<integer>", gettext_noop("Delay DHCP replies for at least number of seconds."), NULL },
  { LOPT_RAPID_COMMIT, OPT_RAPID_COMMIT, NULL, gettext_noop("Enables DHCPv4 Rapid Commit option."), NULL },
  { LOPT_DUMPFILE, ARG_ONE, "<path>", gettext_noop("Path to debug packet dump file"), NULL },
  { LOPT_DUMPMASK, ARG_ONE, "<hex>", gettext_noop("Mask which packets to dump"), NULL },
  { LOPT_SCRIPT_TIME, OPT_LEASE_RENEW, NULL, gettext_noop("Call dhcp-script when lease expiry changes."), NULL },
  { 0, 0, NULL, NULL, NULL }
}; 

#define ARRAY_LEN 1000
int main() {
    int opt_none[ARRAY_LEN] = {-1};
    int opt_one[ARRAY_LEN]  = {-1};
    int opt_dup[ARRAY_LEN]  = {-1};

    int opt_none_i = 0;
    int opt_one_i = 0;
    int opt_dup_i = 0;
    int c = 0;

    // collect 3 lists of options
    for (int i=0;; i++) {
        if (usage[i].opt == 0)
            break;
        //printf("%d ", usage[i].opt);
        for (int ii=0;; ii++) {
            if (opts[ii].name == NULL) {
                printf("\n");
                break;
            }
            
            if (usage[i].opt == opts[ii].val) {
                if (usage[i].rept == ARG_DUP) {
                    printf("%s %d\n", opts[ii].name, 2);
                    opt_dup[opt_dup_i++] = ii;
                } else if (usage[i].rept == ARG_ONE) {
                    printf("%s %d\n", opts[ii].name, 1);
                    opt_one[opt_one_i++] = ii;
                } else {
                    printf("%s %d\n", opts[ii].name, 0);
                    opt_none[opt_none_i++] = ii;
                }
                break;
            }
        }
    }

    // generate python code
    FILE *fd = fopen("options.py", "w");
    fprintf(fd, "# auto generated file from dnsmasq_args.c\n\n");
    fprintf(fd, "# Options that do not take any arguments\n");
    fprintf(fd, "opt_none = [");
    for(int i=0; i < opt_none_i && i<ARRAY_LEN; i++) {
        fprintf(fd, "\"%s\"", opts[opt_none[i]].name);
        if (opt_none[i+1] != 0)
            fprintf(fd, ", ");
    }
    fprintf(fd, "]\n\n");

    fprintf(fd, "# Options that cannot be repeatet (used only once)\n");
    fprintf(fd, "opt_one = [");
    for(int i=0; i < opt_one_i && i<ARRAY_LEN; i++) {
        fprintf(fd, "\"%s\"", opts[opt_one[i]].name);
        if (opt_one[i+1] != 0)
            fprintf(fd, ", ");
    }
    fprintf(fd, "]\n\n");

    fprintf(fd, "# Options that can be repeated\n");
    fprintf(fd, "opt_dup = [");
    for(int i=0; i < opt_dup_i && i<ARRAY_LEN; i++) {
        fprintf(fd, "\"%s\"", opts[opt_dup[i]].name);
        if (opt_dup[i+1] != 0)
            fprintf(fd, ", ");
    }
    fprintf(fd, "]\n\n");
    fclose(fd);

    return 0;
}