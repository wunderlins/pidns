# generate definitions

## generate definitions for repeatable arguments

3 different defintions are available:

- 0: has not arguments (option only)
- 1: option can be used multiple times 
- 2: option can only be used once

an output file with 3 lists of config directive names is generated
in `options.py` by running:

    make


## generate documentation & directives

`options.py` file is needed for this script to work (see above).

This will generate the definition file for all available 
directives according to the currently installed man page.

    MANWIDTH=300 man 8 dnsmasq > man.txt
    ./generate_directives.py ../lib/directives.py

