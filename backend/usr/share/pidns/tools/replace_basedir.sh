#!/usr/bin/env bash

sed -e 's,\$prefix,'$1',' \
    etc/pidns/dnsmasq.conf.template \
    > etc/pidns/dnsmasq.conf 