#!/usr/bin/env python3
#
# generate list of config duirectives from the man page
#
# run:
# MANWIDTH=400 man 8 dnsmasq > man.txt
# ./generate_directives.py > ../directives.py

from pathlib import Path
import os, sys, re
sys.path.insert(0, './lib')
sys.path.insert(0, '../../../lib/pidns')
sys.path.insert(0, '..')
import CfgDirective
config = []

import options

def split_directive(line):
    #special cases
    if line[0:3] == "-6 ":
        return [line[3:]]
    parts = re.split(", +", line)
    if line.startswith("--test"):
        return ["--test"]

    out = []
    for e in parts:
        if e[1:2] != "-":
            #print("-> " + e)
            continue
        #print(e)
        #out.append(e.split(" ")[0])
        out.append(e)

    return out

f = "tools/man.txt"
my_file = Path(f)
if not my_file.is_file():
    f = "man.txt"

with open(f, "r") as a_file:
    SLICE = 0
    SKIPED_OPTIONS_PRE = False

    k = None
    value = None
    descr = ""

    for line in a_file:
        line = line.rstrip()
        #print(line[0:8])

        if line.strip() == "OPTIONS":
            SLICE = 1
            continue
        elif line.strip() == "CONFIG FILE":
            SLICE = 2
            continue
        
        if SLICE == 1:
            if line[0:8] != '       -' and SKIPED_OPTIONS_PRE == False:
                continue
            SKIPED_OPTIONS_PRE = True
            
            # new directive
            if line[0:8] == '       -':
                line = line[7:]
                if k != None:
                    for p in zip(k, value):
                        # find grouping
                        kk = p[0]
                        kv = p[1]

                        # fix brackets
                        if kk[-1:] == "[":
                            kk = kk[0:-1]
                            kv = "[" + kv
                        # fix double brackets
                        if kk[-1:] == "[":
                            kk = kk[0:-1]
                            kv = "[" + kv
                        g = []
                        for e in CfgDirective.grouping:
                            if kk in CfgDirective.grouping[e]:
                                g.append(e)
                                #print(p[0])
                                #print(CfgDirective.grouping[e])
                        if len(g) == 0:
                            g.append(CfgDirective.CfgGroups.OTHER)
                        cfg = CfgDirective.CfgDirective(
                                CfgDirective.CfgType.STRING,
                                kk,
                                kv,
                                None,
                                descr,
                                g
                            )
                        if kk in options.opt_none:
                            cfg.opt = 0
                        elif kk in options.opt_one:
                            cfg.opt = 1
                        elif kk in options.opt_dup:
                            cfg.opt = 2
                        else:
                            cfg.opt = -1
                        #print(cfg)
                        config.append(cfg)

                        #if kk == "log-async":
                        #    sys.stderr.write(str(cfg.to_json()) + "\n")
                        #    sys.stderr.write(str(p[0] in options.opt_dup) + "\n")
                    descr = ""
                
                parts = split_directive(line)
                #sys.stderr.write(line + "\n")
                #sys.stderr.write(str(parts) + "\n")
                k = []
                value = []
                for e in parts:
                    kv = e.split("=")
                    v = ""
                    k.append(kv[0][2:])
                    try:
                        v = kv[1]
                    except: pass
                    value.append(v)
            elif line != "": 
                descr += line.strip() + " "

    for p in zip(k, value):
        # find grouping
        g = []
        for e in CfgDirective.grouping:
            if p[0] in CfgDirective.grouping[e]:
                g.append(e)
                #print(p[0])
                #print(CfgDirective.grouping[e])
        if len(g) == 0:
            g.append(CfgDirective.CfgGroups.OTHER)
        cfg = CfgDirective.CfgDirective(
                CfgDirective.CfgType.STRING,
                p[0],
                p[1],
                None,
                descr,
                g
            )
        if p[0] in options.opt_none:
            cfg.opt = 0
        elif p[0] in options.opt_one:
            cfg.opt = 1
        elif p[0] in options.opt_dup:
            cfg.opt = 2
        else:
            cfg.opt = -1
        config.append(cfg)

    descr = ""


print("from lib.pidns import CfgDirective\n\ndirectives = [")
for e in config:
    #print(e.key + ": " + str(e.value))
    #print(e.description)
    #print("")
    sys.stdout.write("\t")
    e.to_code()
print("]")