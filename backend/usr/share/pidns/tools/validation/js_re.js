export var re = (function() {
    return {
        compile: function(regex, flags) {
            return {
                reg: regex,
                flags: flags,
                reg: new RegExp(regex, flags),
                match: function(str) {
                    return this.reg.test(str)
                }
            }
        }
    }
})();

if (!String.prototype.isnumeric) {
    String.prototype.isnumeric = function() {
        var i = Number.parseInt(this.valueOf())
        if (i.toString() !== this.valueOf())
            return false;

        return true;
    }
}

if (!String.prototype.lower) {
    String.prototype.lower = function() {
        return this.valueOf().toLowerCase();
    }
}
