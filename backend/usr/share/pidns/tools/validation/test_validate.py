#!/usr/bin/env python3
from validate import *

# debug code for js testing
__name__ = "__main__"
if __name__ == "__main__":
    
    # test ip4 validation
    assert(Validate.is_ipv4(None) == False)
    assert(Validate.is_ipv4("") == False)
    assert(Validate.is_ipv4(1) == False)
    assert(Validate.is_ipv4("1") == False)
    assert(Validate.is_ipv4("1.") == False)
    assert(Validate.is_ipv4("-1.1.1.1") == False)
    assert(Validate.is_ipv4("1.1.1.256") == False)
    assert(Validate.is_ipv4("1.1.1.1") == True)

    # test ip6
    assert(Validate.is_ipv6(None) == False)
    assert(Validate.is_ipv6("") == False)
    assert(Validate.is_ipv6(1) == False)
    assert(Validate.is_ipv6("1") == False)
    assert(Validate.is_ipv6("1:2:3:4:%:6:7:8:9") == False)
    assert(Validate.is_ipv6(":abcd") == True)
    assert(Validate.is_ipv6(":efgh") == False)
    assert(Validate.is_ipv6("abcd::abcd") == True)

    # test dnsmasq lease_time format
    assert(Validate.is_lease_time(None) == False)
    assert(Validate.is_lease_time("") == False)
    assert(Validate.is_lease_time(1) == True)
    assert(Validate.is_lease_time("infinite") == True)
    assert(Validate.is_lease_time(0) == True)
    assert(Validate.is_lease_time("9600M") == True)
    assert(Validate.is_lease_time("2w") == True)
    assert(Validate.is_lease_time("w2") == False)

    # test mac addr
    assert(Validate.is_mac(None) == False)
    assert(Validate.is_mac("") == False)
    assert(Validate.is_mac(1) == False)
    assert(Validate.is_mac(":") == False)
    assert(Validate.is_mac("1:2") == False)
    assert(Validate.is_mac("01:02:03:04:05:06") == True)
    assert(Validate.is_mac("01:02:FF:04:05:06") == True)
    assert(Validate.is_mac("01:02:ff:04:05:06") == True)
    assert(Validate.is_mac("01:02:gg:04:05:06") == False)
    assert(Validate.is_mac("01:02: G:04:05:06") == False)

    # test fqdn
    assert(Validate.is_fqdn(None) == False)
    assert(Validate.is_fqdn("") == False)
    assert(Validate.is_fqdn("aaaa") == True)
    assert(Validate.is_fqdn("aa:aa") == False)
    assert(Validate.is_fqdn("aa_aa") == True)
    assert(Validate.is_fqdn("aa_aa.bb_bb") == False)
    assert(Validate.is_fqdn("aa_aa.bb-bb") == True)
    assert(Validate.is_fqdn("aa-aa.bb-bb") == True)
    assert(Validate.is_fqdn("-aa-aa.bb-bb") == False)

    # test netmask
    assert(Validate.is_netmask(None) == False)
    assert(Validate.is_netmask("") == False)
    assert(Validate.is_netmask(0) == True)
    assert(Validate.is_netmask(32) == True)
    assert(Validate.is_netmask(-1) == False)
    assert(Validate.is_netmask("a") == False)

    # test network
    assert(Validate.is_network(None) == False)
    assert(Validate.is_network("") == False)
    assert(Validate.is_network(0) == False)
    assert(Validate.is_network("/24") == False)
    assert(Validate.is_network("24") == False)
    assert(Validate.is_network("192.168.0.0") == False)
    assert(Validate.is_network("192.168.0.0/16") == True)
    assert(Validate.is_network("192.168.0.0/0") == True)
    assert(Validate.is_network("192.168.0.0/33") == False)
