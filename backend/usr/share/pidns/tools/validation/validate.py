import re

class Validate:
    def __init__(self):
        pass

    @classmethod
    def is_ipv4(self, value):
        if value == "" or value == None:
            return False
        
        value = str(value)
        parts = value.split(".")

        # check number of bytes
        l = len(parts)
        if l != 4: 
            #print(f"Too few parts: {l}")
            return False
        
        for e in parts:
            # no empty parts between dots allowed
            if e == "": 
                #print(f"Part {e} empty")
                return False

            # convert to int and compare to string
            i = int(e) 
            if str(i) != e:
                #print(f"unequal {i}: {e}")
                return False
            
            # check range
            if i < 0 or i > 255: 
                #print(f"range error {i}")
                return False
        
        return True
    
    @classmethod
    def is_ipv6(self, value):
        if value == "" or value == None:
            return False
        
        value = str(value)
        if ":" not in value:
            return False
        
        parts = value.split(":")
        if len(parts) > 8:
            return False
        
        for e in parts:
            if e == "":
                continue
            
            if len(e) > 4:
                return False
            
            # all chars in e must contain a HEX digit
            for i in range(len(e)):
                if e[i] not in "0123456789abcdefABCDEF":
                    return False

            i = int(e, 16)
            
            #print(f"Integer {i}")
            if not isinstance(i, int):
                return False
            
            if i > 65535:
                return False
            
        return True

    @classmethod
    def is_lease_time(self, value):
        if value == "" or value == None:
            return False
        
        value = str(value)
        if value.lower() == "infinite":
            return True

        if value.isnumeric():
            return True

        # must be numeric (seconds) or suffixed by
        # - [sS] Seconds
        # - [mM] Minutes
        # - [hH] Hours
        if value[0:-1].isnumeric() and value[-1:] in "sSmMhHdDwW":
            return True

        return False

    @classmethod
    def is_mac(self, value):
        """ check if string is a valid mac address 
        
        in some definitions allow wildcards in mac addresses like
        11:22:33:*, take this into account
        
        """
        if value == "" or value == None:
            return False

        value = str(value)
        parts = value.split(":")
        asterisks = False

        if len(parts) > 6:
            return False
        
        for e in parts:
            if (len(e) > 0 and e[0] == "*") or (len(e) > 1 and e[1] == "*"):
                asterisks = True
                continue

            if len(e) != 2:
                return False

            if e[0] not in "0123456789abcdefABCDEF":
                return False

            if len(e) > 1 and e[1] not in "0123456789abcdefABCDEF":
                return False

        if asterisks == False and len(parts) != 6:
            return False
        
        return True

    @classmethod
    def is_fqdn(self, value):
        """
        # https://stackoverflow.com/questions/2532053/validate-a-hostname-string
        if value[-1] == ".":
            value = value[:-1]
        if len(value) < 1 or len(value) > 253:
            return False
        # FIXME, need manual transformation to javascript regex
        # ldh_re = /^[a-z0-9]([a-z0-9-]{0,61}[a-z0-9])?$/i
        ldh_re = re.compile('^[a-z0-9]([a-z0-9-]{0,61}[a-z0-9])?$', re.IGNORECASE)
        for element in value.split('.'):
            # FIXME, need manual transformation to javascript regex
            # if not element.match(ldh_re):
            if not ldh_re.match(element):
                return False
        """

        #print(f"value: {value}")
        if value == "" or value == None:
            return False

        value = str(value)

        # max length is 253 characters
        if len(value) > 253:
            return False

        # every part must not be longer than 63 characters and min 1 char
        parts = value.split(".")
        for part in parts:
            if not part or len(part) > 63:
                return False

        # now check if all characters entered are valud ascii characters
        # allowed characters as per RFC952, RFC1123
        # - [a-zA-Z0-9\-]
        # - the host part may not start with '-'
        #
        # RFC2181 allows '_' too, only in the host part however
        re_952_1123 = re.compile("^[a-zA-Z0-9-]+$")
        re_2181 = re.compile("^[a-zA-Z0-9_-]+$")

        for i in range(len(parts)):
            # check hostname part, may contain underscores but must not start with '-'
            val = parts[i]
            if i == 0:
                if val[0:1] == '-':
                    return False

                # check the remaining string
                #print(f"re_2181.match({val}): {re_2181.match(val)}")
                if not re_2181.match(val):
                    return False
                
            else:
                # domain name parts
                #print(f"re_952_1123.match({val}): {re_952_1123.match(val)}")
                if not re_952_1123.match(val):
                    return False
        
        return True

    @classmethod
    def is_hostname(self, value):
        if value == "" or value == None:
            return False

        value = str(value)

        # max length is 253 characters
        if len(value) > 253:
            return False
        
        if value[0:1] == '-':
            return False

        re_2181 = re.compile("^[a-zA-Z0-9_-]+$")
        if not re_2181.match(value):
            return False

        return True

    @classmethod
    def is_netmask(self, value):
        if value == "" or value == None:
            return False
        
        value = str(value)

        # is int ?
        if not value.isnumeric():
            return False
        
        # range check
        i = int(value)
        if i < 0 or i > 32:
            return False

        return True

    @classmethod
    def is_network(self, value):
        if value == "" or value == None:
            return False
        
        value = str(value)
        parts = value.split("/")

        if len(parts) != 2:
            return False
        
        if not Validate.is_netmask(parts[1]):
            return False

        if not Validate.is_ipv4(parts[0]):
            return False
        
        return True

