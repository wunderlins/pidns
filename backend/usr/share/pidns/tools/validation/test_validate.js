var re = (function() {
    return {
        compile: function(regex, flags) {
            return {
                reg: regex,
                flags: flags,
                reg: new RegExp(regex, flags),
                match: function(str) {
                    return this.reg.test(str)
                }
            }
        }
    }
})();

if (!String.prototype.isnumeric) {
    String.prototype.isnumeric = function() {
        var i = Number.parseInt(this.valueOf())
        if (i.toString() !== this.valueOf())
            return false;

        return true;
    }
}

if (!String.prototype.lower) {
    String.prototype.lower = function() {
        return this.valueOf().toLowerCase();
    }
}
var _pj;
var __name__;
function _pj_snippets(container) {
    function _assert(comp, msg) {
        function PJAssertionError(message) {
            this.name = "PJAssertionError";
            this.message = (message || "Custom error PJAssertionError");
            if (((typeof Error.captureStackTrace) === "function")) {
                Error.captureStackTrace(this, this.constructor);
            } else {
                this.stack = new Error(message).stack;
            }
        }
        PJAssertionError.prototype = Object.create(Error.prototype);
        PJAssertionError.prototype.constructor = PJAssertionError;
        msg = (msg || "Assertion failed.");
        if ((! comp)) {
            throw new PJAssertionError(msg);
        }
    }
    function in_es6(left, right) {
        if (((right instanceof Array) || ((typeof right) === "string"))) {
            return (right.indexOf(left) > (- 1));
        } else {
            if (((right instanceof Map) || (right instanceof Set) || (right instanceof WeakMap) || (right instanceof WeakSet))) {
                return right.has(left);
            } else {
                return (left in right);
            }
        }
    }
    container["_assert"] = _assert;
    container["in_es6"] = in_es6;
    return container;
}
_pj = {};
_pj_snippets(_pj);
class Validate {
    constructor() {
    }
    static is_ipv4(value) {
        var i, l, parts;
        if (((value === "") || (value === null))) {
            return false;
        }
        value = value.toString();
        parts = value.split(".");
        l = parts.length;
        if ((l !== 4)) {
            return false;
        }
        for (var e, _pj_c = 0, _pj_a = parts, _pj_b = _pj_a.length; (_pj_c < _pj_b); _pj_c += 1) {
            e = _pj_a[_pj_c];
            if ((e === "")) {
                return false;
            }
            i = Number.parseInt(e);
            if ((i.toString() !== e)) {
                return false;
            }
            if (((i < 0) || (i > 255))) {
                return false;
            }
        }
        return true;
    }
    static is_ipv6(value) {
        var i, parts;
        if (((value === "") || (value === null))) {
            return false;
        }
        value = value.toString();
        if ((! _pj.in_es6(":", value))) {
            return false;
        }
        parts = value.split(":");
        if ((parts.length > 8)) {
            return false;
        }
        for (var e, _pj_c = 0, _pj_a = parts, _pj_b = _pj_a.length; (_pj_c < _pj_b); _pj_c += 1) {
            e = _pj_a[_pj_c];
            if ((e === "")) {
                continue;
            }
            if ((e.length > 4)) {
                return false;
            }
            for (var i = 0, _pj_d = e.length; (i < _pj_d); i += 1) {
                if ((! _pj.in_es6(e[i], "0123456789abcdefABCDEF"))) {
                    return false;
                }
            }
            i = Number.parseInt(e, 16);
            if ((! (((typeof i) === "number") || (i instanceof Number)))) {
                return false;
            }
            if ((i > 65535)) {
                return false;
            }
        }
        return true;
    }
    static is_lease_time(value) {
        if (((value === "") || (value === null))) {
            return false;
        }
        value = value.toString();
        if ((value.lower() === "infinite")) {
            return true;
        }
        if (value.isnumeric()) {
            return true;
        }
        if ((value.slice(0, (- 1)).isnumeric() && _pj.in_es6(value.slice((- 1)), "sSmMhHdDwW"))) {
            return true;
        }
        return false;
    }
    static is_mac(value) {
        /*check if string is a valid mac address

        in some definitions allow wildcards in mac addresses like
        11:22:33:*, take this into account

        */
        var asterisks, parts;
        if (((value === "") || (value === null))) {
            return false;
        }
        value = value.toString();
        parts = value.split(":");
        asterisks = false;
        if ((parts.length > 6)) {
            return false;
        }
        for (var e, _pj_c = 0, _pj_a = parts, _pj_b = _pj_a.length; (_pj_c < _pj_b); _pj_c += 1) {
            e = _pj_a[_pj_c];
            if ((((e.length > 0) && (e[0] === "*")) || ((e.length > 1) && (e[1] === "*")))) {
                asterisks = true;
                continue;
            }
            if ((e.length !== 2)) {
                return false;
            }
            if ((! _pj.in_es6(e[0], "0123456789abcdefABCDEF"))) {
                return false;
            }
            if (((e.length > 1) && (! _pj.in_es6(e[1], "0123456789abcdefABCDEF")))) {
                return false;
            }
        }
        if (((asterisks === false) && (parts.length !== 6))) {
            return false;
        }
        return true;
    }
    static is_fqdn(value) {
        /*
        # https://stackoverflow.com/questions/2532053/validate-a-hostname-string
        if value[-1] == ".":
        value = value[:-1]
        if len(value) < 1 or len(value) > 253:
        return False
        # FIXME, need manual transformation to javascript regex
        # ldh_re = /^[a-z0-9]([a-z0-9-]{0,61}[a-z0-9])?$/i
        ldh_re = re.compile('^[a-z0-9]([a-z0-9-]{0,61}[a-z0-9])?$', re.IGNORECASE)
        for element in value.split('.'):
        # FIXME, need manual transformation to javascript regex
        # if not element.match(ldh_re):
        if not ldh_re.match(element):
        return False
        */
        var parts, re_2181, re_952_1123, val;
        if (((value === "") || (value === null))) {
            return false;
        }
        value = value.toString();
        if ((value.length > 253)) {
            return false;
        }
        parts = value.split(".");
        for (var part, _pj_c = 0, _pj_a = parts, _pj_b = _pj_a.length; (_pj_c < _pj_b); _pj_c += 1) {
            part = _pj_a[_pj_c];
            if (((! part) || (part.length > 63))) {
                return false;
            }
        }
        re_952_1123 = re.compile("^[a-zA-Z0-9-]+$");
        re_2181 = re.compile("^[a-zA-Z0-9_-]+$");
        for (var i = 0, _pj_a = parts.length; (i < _pj_a); i += 1) {
            val = parts[i];
            if ((i === 0)) {
                if ((val.slice(0, 1) === "-")) {
                    return false;
                }
                if ((! re_2181.match(val))) {
                    return false;
                }
            } else {
                if ((! re_952_1123.match(val))) {
                    return false;
                }
            }
        }
        return true;
    }
    static is_hostname(value) {
        var re_2181;
        if (((value === "") || (value === null))) {
            return false;
        }
        value = value.toString();
        if ((value.length > 253)) {
            return false;
        }
        if ((value.slice(0, 1) === "-")) {
            return false;
        }
        re_2181 = re.compile("^[a-zA-Z0-9_-]+$");
        if ((! re_2181.match(value))) {
            return false;
        }
        return true;
    }
    static is_netmask(value) {
        var i;
        if (((value === "") || (value === null))) {
            return false;
        }
        value = value.toString();
        if ((! value.isnumeric())) {
            return false;
        }
        i = Number.parseInt(value);
        if (((i < 0) || (i > 32))) {
            return false;
        }
        return true;
    }
    static is_network(value) {
        var parts;
        if (((value === "") || (value === null))) {
            return false;
        }
        value = value.toString();
        parts = value.split("/");
        if ((parts.length !== 2)) {
            return false;
        }
        if ((! Validate.is_netmask(parts[1]))) {
            return false;
        }
        if ((! Validate.is_ipv4(parts[0]))) {
            return false;
        }
        return true;
    }
}
__name__ = "__main__";
if ((__name__ === "__main__")) {
    _pj._assert((Validate.is_ipv4(null) === false), null);
    _pj._assert((Validate.is_ipv4("") === false), null);
    _pj._assert((Validate.is_ipv4(1) === false), null);
    _pj._assert((Validate.is_ipv4("1") === false), null);
    _pj._assert((Validate.is_ipv4("1.") === false), null);
    _pj._assert((Validate.is_ipv4("-1.1.1.1") === false), null);
    _pj._assert((Validate.is_ipv4("1.1.1.256") === false), null);
    _pj._assert((Validate.is_ipv4("1.1.1.1") === true), null);
    _pj._assert((Validate.is_ipv6(null) === false), null);
    _pj._assert((Validate.is_ipv6("") === false), null);
    _pj._assert((Validate.is_ipv6(1) === false), null);
    _pj._assert((Validate.is_ipv6("1") === false), null);
    _pj._assert((Validate.is_ipv6("1:2:3:4:%:6:7:8:9") === false), null);
    _pj._assert((Validate.is_ipv6(":abcd") === true), null);
    _pj._assert((Validate.is_ipv6(":efgh") === false), null);
    _pj._assert((Validate.is_ipv6("abcd::abcd") === true), null);
    _pj._assert((Validate.is_lease_time(null) === false), null);
    _pj._assert((Validate.is_lease_time("") === false), null);
    _pj._assert((Validate.is_lease_time(1) === true), null);
    _pj._assert((Validate.is_lease_time("infinite") === true), null);
    _pj._assert((Validate.is_lease_time(0) === true), null);
    _pj._assert((Validate.is_lease_time("9600M") === true), null);
    _pj._assert((Validate.is_lease_time("2w") === true), null);
    _pj._assert((Validate.is_lease_time("w2") === false), null);
    _pj._assert((Validate.is_mac(null) === false), null);
    _pj._assert((Validate.is_mac("") === false), null);
    _pj._assert((Validate.is_mac(1) === false), null);
    _pj._assert((Validate.is_mac(":") === false), null);
    _pj._assert((Validate.is_mac("1:2") === false), null);
    _pj._assert((Validate.is_mac("01:02:03:04:05:06") === true), null);
    _pj._assert((Validate.is_mac("01:02:FF:04:05:06") === true), null);
    _pj._assert((Validate.is_mac("01:02:ff:04:05:06") === true), null);
    _pj._assert((Validate.is_mac("01:02:gg:04:05:06") === false), null);
    _pj._assert((Validate.is_mac("01:02: G:04:05:06") === false), null);
    _pj._assert((Validate.is_fqdn(null) === false), null);
    _pj._assert((Validate.is_fqdn("") === false), null);
    _pj._assert((Validate.is_fqdn("aaaa") === true), null);
    _pj._assert((Validate.is_fqdn("aa:aa") === false), null);
    _pj._assert((Validate.is_fqdn("aa_aa") === true), null);
    _pj._assert((Validate.is_fqdn("aa_aa.bb_bb") === false), null);
    _pj._assert((Validate.is_fqdn("aa_aa.bb-bb") === true), null);
    _pj._assert((Validate.is_fqdn("aa-aa.bb-bb") === true), null);
    _pj._assert((Validate.is_fqdn("-aa-aa.bb-bb") === false), null);
    _pj._assert((Validate.is_netmask(null) === false), null);
    _pj._assert((Validate.is_netmask("") === false), null);
    _pj._assert((Validate.is_netmask(0) === true), null);
    _pj._assert((Validate.is_netmask(32) === true), null);
    _pj._assert((Validate.is_netmask((- 1)) === false), null);
    _pj._assert((Validate.is_netmask("a") === false), null);
    _pj._assert((Validate.is_network(null) === false), null);
    _pj._assert((Validate.is_network("") === false), null);
    _pj._assert((Validate.is_network(0) === false), null);
    _pj._assert((Validate.is_network("/24") === false), null);
    _pj._assert((Validate.is_network("24") === false), null);
    _pj._assert((Validate.is_network("192.168.0.0") === false), null);
    _pj._assert((Validate.is_network("192.168.0.0/16") === true), null);
    _pj._assert((Validate.is_network("192.168.0.0/0") === true), null);
    _pj._assert((Validate.is_network("192.168.0.0/33") === false), null);
}

//# sourceMappingURL=test_validate_tmp.js.map
