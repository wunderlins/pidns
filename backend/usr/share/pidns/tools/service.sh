#!/usr/bin/env bash

#uwsgi --socket 0.0.0.0:8080 \
#    --protocol=http \
#    --manage-script-name \
#    --mount /=web:app

if [[ "$1" == "start" ]]; then
#    uwsgi --http-socket :8088 \
#        --plugin python3 \
#        --chdir "$PWD" \
#        --module=$PWD/usr/libexec/pidns-web \
#        --mount /=$PWD/usr/libexec/pidns-web:app \
#        --master --processes 4 --threads 2 \
#        --pidfile "$PWD/var/pidns/uwsgi.pid" \
#        --daemonize2 "$PWD/var/pidns/uwsgi.log"

	uwsgi --http-socket :8088 \
		--plugin python3 \
		--callable app \
		--file $PWD/usr/libexec/pidns-web \
		--master --processes 1 --threads 4 \
		--pidfile "$PWD/var/pidns/uwsgi.pid" \
		--daemonize2 "$PWD/var/pidns/uwsgi.log"
fi

if [[ "$1" == "stop" ]]; then
    pid=$(cat "$PWD/var/pidns/uwsgi.pid")
    echo "Terminating pid $pid"
    kill -9 $pid
fi
#    --stats 127.0.0.1:9191
#        --callable=app \
#		--async 4 \
#		--plugin python3 \
#		--wsgi-file $PWD/usr/libexec/pidns-web \
#		--module $PWD/usr/libexec/pidns-web:app \
