.PHONY: all

all: dnsmasq_definitions validators config_basedir build_frontend 

build_frontend:
	@echo == building static SPA
	cd frontend && ./build.sh

config_basedir:
	@echo == Adjusting config file paths
	$(shell cd backend && ./usr/share/pidns/tools/replace_basedir.sh $$PWD)

validators:
	@echo == building validators for frontend
	$(MAKE) -C backend/usr/share/pidns/tools/validation

dnsmasq_definitions:
	@echo == building dnsmasq definitions
	$(MAKE) -C backend/usr/share/pidns/tools